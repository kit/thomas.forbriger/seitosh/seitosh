#!/bin/bash
# this is <checklibs.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2010 by Thomas Forbriger (BFO Schiltach) 
# 
# check whether library packages required by Seitosh are installed
# The script is designed for openSuSE installations, using the rpm package
# manager
#
# ----
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# ----
# 
# REVISIONS and CHANGES 
#    20/12/2010   V1.0   Thomas Forbriger
#    02/04/2019   V1.1   adjust to be used with OpenSuSE 15.0
# 
# ============================================================================
# location of rpm package manager
RPMCMD=/bin/rpm
# ============================================================================
#
# check a single package
#
# parameters:
# $1: comment describing the packages to be checked
# $2: regex pattern to identify package(s)
checkpkg()
{
  echo 
  echo checking "$1"
  ${RPMCMD} -qa | egrep "$2" | sed -e 's/^/  /'
}

# ============================================================================
#
# report a reference installation on which Seitosh compiles successfully
#
refout()
{
  echo 
  echo "Output on my system (as a reference)"
  echo "------------------------------------"
  echo
  cat << HERE
This system is:
Linux patty 4.12.14-lp150.12.48-default #1 SMP Tue Feb 12 14:01:48 UTC 2019 (268f014) x86_64 x86_64 x86_64 GNU/Linux
LSB Version:	n/a
Distributor ID:	openSUSE
Description:	openSUSE Leap 15.0
Release:	15.0
Codename:	n/a

checking the BOOST libboost regex
  libboost_regex1_66_0-devel-1.66.0-lp150.3.3.x86_64
  libboost_regex1_66_0-1.66.0-lp150.3.3.x86_64

checking the BOOST libboost development package
  libboost_headers1_66_0-devel-1.66.0-lp150.3.3.x86_64
  libboost_regex1_66_0-devel-1.66.0-lp150.3.3.x86_64

checking the LAPACK library
  lapack-devel-3.5.0-lp150.3.3.1.x86_64
  liblapacke3-3.5.0-lp150.3.3.1.x86_64
  liblapack3-32bit-3.5.0-lp150.3.3.1.x86_64
  liblapack3-3.5.0-lp150.3.3.1.x86_64
  lapack-man-3.5.0-lp150.3.3.1.noarch
  lapack-devel-32bit-3.5.0-lp150.3.3.1.x86_64

checking the BLAS library
  openblas-devel-0.2.20-lp150.5.4.x86_64
  libblas3-3.5.0-lp150.3.3.1.x86_64
  blas-devel-3.5.0-lp150.3.3.1.x86_64
  libblas3-32bit-3.5.0-lp150.3.3.1.x86_64
  openblas-devel-headers-0.2.20-lp150.5.4.noarch
  cblas-devel-20110120-lp150.1.14.x86_64
  blas-man-3.5.0-lp150.3.3.1.noarch

checking the GSL library
  gsl-2.4-lp150.1.9.x86_64

checking the GSL library development package
  gsl-devel-2.4-lp150.1.9.x86_64

checking the FFTW3 library
  libfftw3-3-3.3.6-lp150.3.3.x86_64

checking the FFTW3 library development package
  fftw3-devel-3.3.6-lp150.3.3.x86_64

checking the GNU C compiler
  gcc7-32bit-7.3.1+r258812-lp150.2.10.x86_64
  gcc7-fortran-7.3.1+r258812-lp150.2.10.x86_64
  gcc-7-lp150.1.50.x86_64
  gcc7-7.3.1+r258812-lp150.2.10.x86_64
  gcc-c++-7-lp150.1.50.x86_64
  gcc7-info-7.3.1+r258812-lp150.2.10.noarch
  gcc-info-7-lp150.1.50.x86_64
  gcc-32bit-7-lp150.1.50.x86_64
  gcc7-c++-7.3.1+r258812-lp150.2.10.x86_64
  gcc-fortran-7-lp150.1.50.x86_64

checking the GNU C++ compiler
  gcc7-c++-7.3.1+r258812-lp150.2.10.x86_64

checking the GNU Fortran compiler
  gcc7-fortran-7.3.1+r258812-lp150.2.10.x86_64

checking the GNU Fortran library
  libgfortran4-32bit-7.3.1+r258812-lp150.2.10.x86_64
  libgfortran4-7.3.1+r258812-lp150.2.10.x86_64

checking the GNU stdc++ library development package
  libstdc++-devel-7-lp150.1.50.x86_64
  libstdc++6-devel-gcc7-7.3.1+r258812-lp150.2.10.x86_64

checking the f2c compiler
  f2c-0.11-lp150.1197.10.x86_64
  f2c-32bit-0.11-lp150.1197.10.x86_64

checking the GNU make utility
  make-4.2.1-lp150.6.3.1.x86_64
  makeinfo-6.5-lp150.4.14.x86_64
  make-lang-4.2.1-lp150.6.3.1.noarch

checking the doxygen documentation generator
  doxygen-1.8.14-lp150.1.6.x86_64
HERE
}
# ============================================================================
echo This script checks whether library packages required by Seitosh
echo source code are installed. The script is designed for openSuSE
echo installations using the rpm package manager. 
echo
echo
refout
echo
echo ------------------------------------------------------------------
echo Checking your system:
test -x ${RPMCMD} || { echo package manager ${RPMCMD} is missing; exit 2; }
echo
echo This system is:
uname -a
if test -x /usr/bin/lsb_release
then
  /usr/bin/lsb_release -a
else
  echo /usr/bin/lsb_release is missing
fi
if test -r /etc/SuSE-release
then
  echo The installed SuSE release is:
  cat /etc/SuSE-release
fi

checkpkg "the BOOST libboost regex" "boost.*regex"
checkpkg "the BOOST libboost development package" "boost.*devel"
checkpkg "the LAPACK library" "lapack"
checkpkg "the BLAS library" "(blas-)|(libblas)"
checkpkg "the GSL library" "gsl-[[:digit:]]"
checkpkg "the GSL library development package" "gsl-devel"
checkpkg "the FFTW3 library" "fftw3-[[:digit:]]"
checkpkg "the FFTW3 library development package" "fftw3-devel"
checkpkg "the GNU C compiler" "^gcc([[:digit:]])?(-32bit)?"
checkpkg "the GNU C++ compiler" "^gcc([[:digit:]])-c++(-32bit)?"
checkpkg "the GNU Fortran compiler" "^gcc([[:digit:]])-fortran(-32bit)?"
checkpkg "the GNU Fortran library" "libgfortran"
checkpkg "the GNU stdc++ library development package" "libstdc++.*devel"
checkpkg "the f2c compiler" "f2c"
checkpkg "the GNU make utility" "^make"
checkpkg "the doxygen documentation generator" "doxygen"

# ----- END OF checklibs.sh ----- 
