#!/bin/sh
# this is <syslogtest.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
# 
# Test syslog configuration
# 
# ----
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
# ----
#
# REVISIONS and CHANGES 
#    19/06/2019   V1.0   Thomas Forbriger
# 
# ============================================================================
#
# produce syslog messages like produced by DL1logger
./tester -syslog "syslog test: $(date)" -v
./tester -syslog "syslog test: $(date)" -loginfo -v
./tester -syslog "syslog test: $(date)" -logemerg -v
#
# produce syslog messages like launchDL1logger.sh and DL1logger.sh
echo "check syslog" | /bin/logger -i -p user.info -t "DL1logger" 2>&1
#
# run chrony check
./chronyreport.sh
#
# ----- END OF syslogtest.sh ----- 
