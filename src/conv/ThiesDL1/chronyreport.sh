#!/bin/sh
# this is <chronyreport.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
# 
# report chrony status
# 
# replaces ntpreport.sh on systems where chronyd has replaced ntpd
#
# ----
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
# ----
#
# REVISIONS and CHANGES 
#    07/06/2019   V1.0   Thomas Forbriger
# 
# ============================================================================
#
echo "run $0" | /bin/logger -i -p user.notice -t "DL1logger" 2>&1
echo "check status of time sources" | /bin/logger -i -p user.notice -t "DL1logger" 2>&1
/usr/bin/chronyc sources | /bin/logger -i -p user.notice -t "DL1logger" 2>&1
/usr/bin/chronyc sourcestats | /bin/logger -i -p user.notice -t "DL1logger" 2>&1

# ----- END OF chronyreport.sh ----- 
