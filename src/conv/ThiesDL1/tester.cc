/*! \file tester.cc
 * \brief a small program to test some functions
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \since 24/03/2014
 * \date 19/06/2019
 * 
 * a small program to test some functions
 * 
 * Copyright (c) 2008, 2014, 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * ----
 * 
 * REVISIONS and CHANGES 
 *  - 28/11/2008   V1.0   Thomas Forbriger
 *  - 19/06/2019   V1.1   add syslog test function
 * 
 * ============================================================================
 */
#define TESTER_VERSION \
  "TESTER   V2019-06-19   a small program to test some functions"

#include <iostream>
#include <list>
#include <tfxx/commandline.h>
#include <tfxx/stringfunc.h>
#include <tfxx/filestatus.h>
#include "thiesdl1.h"
#include "functions.h"
#include "memory.h"
#include "logger.h"

typedef std::list<std::string> Tlistofstring;

using std::cout;
using std::cerr;
using std::endl;

/*----------------------------------------------------------------------*/

void checkresult(const std::string& path,
                 const std::string& test,
                 const bool& result)
{
  cout << path << " is ";
  if (!result) { cout << "NOT "; }
  cout << test << endl;
}

/*----------------------------------------------------------------------*/

int main(int iargc, char* argv[])
{

  // define usage information
  char usage_text[]=
  {
    TESTER_VERSION "\n"
    "usage: tester [-v] [-pattern p] [-Id] [-checkdatepattern p]" "\n"
    "              [-checkfileexists f] [-preppath p] [-active]\n"
    "   or: tester --help|-h" "\n"
  };

  // define full help text
  char help_text[]=
  {
    TESTER_VERSION
    "flags:\n"
    "-v           be verbose\n"
    "-active      set active flag for dl1::mkpathname in test \"-preppath\"\n"
    "-loginfo     set syslog level to info\n"
    "-logemerg    set syslog level to emerg\n"
    "\n"
    "tests:\n"
    "-pattern p   test pattern replacement function\n"
    "             dl1::patsubstdate\n"
    "-Id          dump list of CVSIDS\n"
    "-checkdatepattern p      test function\n"
    "                         dl1::datetemplatespresent\n"
    "-checkfileexists f       test functions in tfxx::file\n"
    "-preppath p  test function\n"
    "             dl1::mkpathname\n"
    "-syslog s    send string \"s\" to syslog\n"
  };

  // define commandline options
  using namespace tfxx::cmdline;
  static Declare options[]= 
  {
    // 0: print help
    {"help",arg_no,"-"},
    // 1: verbose mode
    {"v",arg_no,"-"},
    // 2: test pattern
    {"pattern",arg_yes,"-"},
    // 3: report source code ids
    {"Id",arg_no,"-"},
    // 4: check data pattern
    {"checkdatepattern",arg_yes,"-"},
    // 5: check if file exists
    {"checkfileexists",arg_yes,"-"},
    // 6: prepare file path
    {"preppath",arg_yes,"-"},
    // 7: set active flag
    {"active",arg_no,"-"},
    // 8: send string to syslog
    {"syslog",arg_yes,"-"},
    // 9: set syslog level to info
    {"loginfo",arg_no,"-"},
    // 10: set syslog level to emerg
    {"logemerg",arg_no,"-"},
    {NULL}
  };

  // no arguments? print usage...
  if (iargc<2) 
  {
    cerr << usage_text << endl;
    cerr << "the program expects at least one argument to continue" << endl;
    exit(0);
  }

  // collect options from commandline
  Commandline cmdline(iargc, argv, options);

  // help requested? print full help text...
  if (cmdline.optset(0))
  {
    cerr << usage_text << endl;
    cerr << help_text << endl;
    exit(0);
  }

  /*======================================================================*/

  if (cmdline.optset(3))
  {
    cout << "source code version" << endl;
    int i=0;
    while (dl1::CVSIDS[i]!=0)
    {
      cout << dl1::CVSIDS[i] << endl;
      ++i;
    }
  }

  /*======================================================================*/

  if (cmdline.optset(2))
  {
    std::string pattern=cmdline.string_arg(2);
    cout << "pattern with templates to be replaced: ";
    cout << pattern << endl;
    pattern=dl1::patsubstdate(pattern, libtime::utc());
    cout << "result: " << pattern << endl;
  }

  /*======================================================================*/

  if (cmdline.optset(4))
  {
    std::string pattern=cmdline.string_arg(4);
    cout << "pattern with templates: ";
    cout << pattern << endl;
    cout << "pattern ";
    if (dl1::datetemplatespresent(pattern))
    {
      cout << "contains";
    }
    else
    {
      cout << "does not contain";
    }
    cout << " all required date templates." << endl;
  }

  /*======================================================================*/

  if (cmdline.optset(5))
  {
    std::string pattern=cmdline.string_arg(5);
    checkresult(pattern, "existing", tfxx::file::exists(pattern.c_str()));
    checkresult(pattern, "creatable", tfxx::file::creatable(pattern.c_str()));
    checkresult(pattern, "writable", tfxx::file::writable(pattern.c_str()));
    checkresult(pattern, "readable", tfxx::file::readable(pattern.c_str()));
  }

  /*======================================================================*/

  if (cmdline.optset(6))
  {
    std::string pattern=cmdline.string_arg(6);
    cout << "file path pattern: " << pattern << endl;
    std::string pathname=dl1::mkpathname(pattern, libtime::utc(), "test", 
                                         cmdline.optset(7));
    cout << "file path: " << pathname << endl;
  }

  /* ====================================================================== */
  if (cmdline.optset(8))
  {
    dl1::Logger::setident("DL1logger");
    dl1::Logger::beverbose(cmdline.optset(1));
    if (cmdline.optset(9))
    {
      dl1::Logger(dl1::log_info) << TESTER_VERSION;
      dl1::Logger(dl1::log_info) << "logging at info level";
      dl1::Logger(dl1::log_info) << cmdline.string_arg(8);
    }
    else if (cmdline.optset(10))
    {
      dl1::Logger(dl1::log_emerg) << TESTER_VERSION;
      dl1::Logger(dl1::log_emerg) << "logging at emerg level";
      dl1::Logger(dl1::log_emerg) << cmdline.string_arg(8);
    }
    else
    {
      dl1::Logger() << TESTER_VERSION;
      dl1::Logger() << "logging at default level";
      dl1::Logger() << cmdline.string_arg(8);
    }
  }

}

/* ----- END OF tester.cc ----- */
