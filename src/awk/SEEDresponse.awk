#!/bin/gawk -f
# this is <SEEDresponse.awk>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2005 by Thomas Forbriger (BFO Schiltach) 
# 
# parse a SEED response file an translate poles and zeroes
#
# ----
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# ----
#
# You can select specific stations and/or channels by placing regular
# expressions in the variables STATION and/or CHANNEL. Example:
#
#   SEEDresponse.awk -v STATION="AAE|GRFO|ANMO" -v CHANNEL="LH." filename
# 
# use PRINTCOEFF=1 to select printing of polynomial coefficients
#
# REVISIONS and CHANGES 
#    27/09/2005   V1.0   Thomas Forbriger
#                        scans SZGRF response data
#    12/01/2006   V1.1   extracts channel information and dat information
#    29/10/2007   V1.2   support for additional blockettes and
#                        response type B
#    07/10/2009   V1.3   report any unprocessed line
#    04/10/2011   V1.4   corrected error when deriving damping for the Laplace
#                        transform given by poles in rad/s
# 
# ============================================================================
#
# Each line in a response file produced by rdseed starts with an identifier
# string like B050F03. This identifier specifies the blockette from which
# information was taken and the field within the blockette. In the example
# above, the blockette 50 was the source of information (called "type 50"
# below).
#
# The program currently cannot handle dictionary headers.
# 
# ============================================================================
#
# return absolute
function abso(v) { rv= v < 0. ? -v : v; return rv; }
# ----------------------------------------------------------------------------
# check whether two poles give a complex pair
function complexpair(r1,i1,r2,i2,type)
{
  ispair=0;
  ai1=abso(i1);
  ai2=abso(i2);
  ar1=abso(r1);
  ar2=abso(r2);
  sr2= ar2 < 1.e-20 ? 1. : ar2;
  si2= ai2 < 1.e-20 ? 1. : ai2;
  if ((type == "A" ) || (type == "B" ))
  {
    
    d1=abso(1.-(ar1/sr2));
    d2=abso(1.-(ai1/si2));
    if ( ( d1 < 1.e-20 ) && ( d2 < 1.e-20 ) &&
         ( ai1 > 1.e-20 ) && ( ai2 > 1.e-20 ) )
    { ispair=1; }
  }
  else if ( type == "C" )
  {
    printf ("ERROR (complexpair): cannot handle type C (undefined)\n");
  }
  else if ( type == "D" )
  {
    printf ("ERROR (complexpair): cannot handle type D (Z transform)\n");
  }
  else
  {
    printf ("ERROR (complexpair): unknown response type \"%s\"!\n", type);
  }
  return ispair;
}
# ----------------------------------------------------------------------------
# return damping as a fraction of critical
function damping(r,i,type)
{ 
  damp=-1.;
  if ( type == "A" )
  {
    damp=abso(r)/angularfrequency(r,i);
  }
  else if ( type == "B" )
  {
    damp=abso(r)/frequency(r,i,type);
  }
  else if ( type == "C" )
  {
    printf ("ERROR (damping): cannot handle type C (undefined)\n");
  }
  else if ( type == "D" )
  {
    printf ("ERROR (damping): cannot handle type D (Z transform)\n");
  }
  else
  {
    printf ("ERROR (damping): unknown response type \"%s\"!\n", type);
  }
  return damp;
}
# ----------------------------------------------------------------------------
# return angular frequency of pole
function angularfrequency(r,i)
{ return sqrt(r*r+i*i); }
# ----------------------------------------------------------------------------
# return frequency of pole
function frequency(r,i,type)
{ 
  fr=-1.;
  if ( type == "A" )
  {
    fr=angularfrequency(r,i)/(2.*3.1415926535897931159); 
  }
  else if ( type == "B" )
  {
    fr=angularfrequency(r,i);
  }
  else if ( type == "C" )
  {
    printf ("ERROR (frequency): cannot handle type C (undefined)\n");
  }
  else if ( type == "D" )
  {
    printf ("ERROR (frequency): cannot handle type D (Z transform)\n");
  }
  else
  {
    printf ("ERROR (frequency): unknown response type \"%s\"!\n", type);
  }
  return fr;
}
# ----------------------------------------------------------------------------
function period(r,i,type)
{
  f=frequency(r,i,type);
  if ( f < 1.e-20 )
  {
    return "inf";
  }
  return 1./f;
}
# ----------------------------------------------------------------------------
function get_header()
{
  STATION_CODE[NSTAGE]=STATION_CODE_S;
  LOCATION_CODE[NSTAGE]=LOCATION_CODE_S;
  NETWORK_CODE[NSTAGE]=NETWORK_CODE_S;
  STATION_NAME[NSTAGE]=STATION_NAME_S;
  CHANNEL_NAME[NSTAGE]=CHANNEL_NAME_S;
  INSTRUMENT_NAME[NSTAGE]=INSTRUMENT_NAME_S;
  LATITUDE[NSTAGE]=LATITUDE_S;
  LONGITUDE[NSTAGE]=LONGITUDE_S;
  ELEVATION[NSTAGE]=ELEVATION_S;
  DEPTH[NSTAGE]=DEPTH_S;
  START_DATE[NSTAGE]=START_DATE_S;
  END_DATE[NSTAGE]=END_DATE_S;
  retrurn 0;
}
# ----------------------------------------------------------------------------
function all_after(n)
{
  retval="";
  for (i=n; i<=NF; ++i)
  {
    retval=retval $i " ";
  }
  return retval;
}
# ----------------------------------------------------------------------------
function print_header_string(n,c)
{
  if ( c != NSP )
  {
    printf("%20s: %s\n", n, c);
  }
  return 0;
}
# ----------------------------------------------------------------------------
function print_header_value(n,c)
{
  if ( c != NSP )
  {
    printf("%20s: %15.8f\n", n, c);
  }
  return 0;
}
# ----------------------------------------------------------------------------
function print_number(n,c)
{ printf("%25s: %15d\n", n, c); }
# ----------------------------------------------------------------------------
function print_value(n,c)
{ printf("%25s: %15.8f\n", n, c); }
# ----------------------------------------------------------------------------
function print_value_w_units(n,c,u)
{ printf("%25s: %15.8f %s\n", n, c,u); }
# ----------------------------------------------------------------------------
function warning_unprocessed_blockette(n)
{
  printf("WARNING: %-12s unprocessed Blockette %2.2d (record #%d)\n", 
    $1, n, FNR);
  return 0;
}
# ----------------------------------------------------------------------------
function warning_unprocessed_field(n)
{
  printf("WARNING: %-12s unprocessed Field of Blockette %2.2d (record #%d)\n", 
    $1, n, FNR);
  return 0;
}
# ----------------------------------------------------------------------------
function print_header(i)
{
#  printf("tabo: %s\n", SELECT_TABLE_OUTPUT);
  if (SELECT_TABLE_OUTPUT) 
  {
    printf("TAB: %s & %s & %s & %s\n",
      STATION_CODE[i], CHANNEL_NAME[i], 
      START_DATE[i], END_DATE[i]);
    printf("TAB: %s & %s & %s & %s & %s\n",
      STATION_CODE[i], CHANNEL_NAME[i], 
      STATION_CODE[i], CHANNEL_NAME[i], 
      STATION_NAME[i]);
    printf("TAB: %s & %s & %11.4f & %11.4f & %11.4f & %11.4f\n",
      STATION_CODE[i], CHANNEL_NAME[i], 
      LATITUDE[i], LONGITUDE[i], ELEVATION[i], DEPTH[i]);
    printf("TAB: %s & %s & %s\\\\\n",
      STATION_CODE[i], CHANNEL_NAME[i], 
      INSTRUMENT_NAME[i]);
  }
  print_header_string("station code", STATION_CODE[i]);
  print_header_string("network code", NETWORK_CODE[i]);
  print_header_string("station name", STATION_NAME[i]);
  print_header_string("channel name", CHANNEL_NAME[i]);
  print_header_string("location code", LOCATION_CODE[i]);
  print_header_string("instrument name", INSTRUMENT_NAME[i]);
  print_header_value("latitude", LATITUDE[i]);
  print_header_value("longitude", LONGITUDE[i]);
  print_header_value("elevation", ELEVATION[i]);
  print_header_value("depth", DEPTH[i]);
  print_header_string("start date", START_DATE[i]);
  print_header_string("end date", END_DATE[i]);
  retrurn 0;
}
# ----------------------------------------------------------------------------
function match_station(s,c)
{
  if ( length(SELECT_STATION) == 0 )
  { 
    SRETVAL=1;
  }
  else
  {
    SRETVAL=match(s, SELECT_STATION);
  }
  if ( length(SELECT_CHANNEL) == 0 )
  { 
    CRETVAL=1;
  }
  else
  {
    CRETVAL=match(c, SELECT_CHANNEL);
  }
  return (SRETVAL && CRETVAL);
}
# ============================================================================
BEGIN {
# initialize station and channel selection
# is STATION and/or CHANNEL are not set, any station and/or channel are read
  SELECT_STATION=STATION;
  SELECT_CHANNEL=CHANNEL;
# is TABLE=1, table (debug) output will be active
  SELECT_TABLE_OUTPUT=TABLE;
# if true: print polynomial coefficients
  SELECT_COEFF_OUTPUT=PRINTCOEFF;
# overall number of stages read from input
  NSTAGE=0;
# set any information not yet given to NSP
  NSP="not specified";
  STATION_CODE_S=NSP;
  NETWORK_CODE_S=NSP;
  LOCATION_CODE_S=NSP;
  STATION_NAME_S=NSP;
  CHANNEL_NAME_S=NSP;
  INSTRUMENT_NAME_S=NSP;
  LATITUDE_S=NSP;
  LONGITUDE_S=NSP;
  ELEVATION_S=NSP;
  DEPTH_S=NSP;
  START_DATE_S=NSP;
  END_DATE_S=NSP;
}
# ============================================================================
# type 050 - station identifier
#
# station code for new sequence
/^B050F03/ { 
    STATION_CODE_S=$3; 
    NETWORK_CODE_S=NSP;
    LOCATION_CODE_S=NSP;
    STATION_NAME_S=NSP;
    CHANNEL_NAME_S=NSP;
    INSTRUMENT_NAME_S=NSP;
    LATITUDE_S=NSP;
    LONGITUDE_S=NSP;
    ELEVATION_S=NSP;
    DEPTH_S=NSP;
    START_DATE_S=NSP;
    END_DATE_S=NSP;
    next;
  }
/^B050F16/ { 
  NETWORK_CODE_S=$3; 
  next;
}
/^B050F09/ { 
  NSUBSTRINGS=split($0, linear);
  STATION_NAME_S="";
  for (i=3; i<=NSUBSTRINGS; ++i)
  {
    STATION_NAME_S = STATION_NAME_S linear[i] " ";
  }
  next;
}
/^B050F/ { warning_unprocessed_field(50); next; }
# ============================================================================
# type 051 - station comment
/^B051F/ { warning_unprocessed_blockette(51); next; }
# ============================================================================
# type 052 - channel identifier
/^B052F03/ { 
  LOCATION_CODE_S=$3; 
  next;
}
/^B052F04/ { 
  CHANNEL_NAME_S=$3; 
  next;
}
/^B052F06/ { 
  NSUBSTRINGS=split($0, linear);
  INSTRUMENT_NAME_S="";
  for (i=5; i<=NSUBSTRINGS; ++i)
  {
    INSTRUMENT_NAME_S = INSTRUMENT_NAME_S linear[i] " ";
  }
  next;
}
/^B052F10/ { LATITUDE_S=$3; next; }
/^B052F11/ { LONGITUDE_S=$3; next; }
/^B052F12/ { ELEVATION_S=$3; next; }
/^B052F13/ { DEPTH_S=$4; next; }
/^B052F22/ { START_DATE_S=all_after(4); next; }
/^B052F23/ { END_DATE_S=all_after(4); next; }
/^B052F/ { warning_unprocessed_field(52); next; }
# ============================================================================
# type 053 - response (poles & zeros)
/^B053F03/ { 
  NSTAGE++;
  TRANSFER_TYPE[NSTAGE]=$5; 
  TRANSFER_ALLTYPE[NSTAGE]=all_after(5); 
  SEQUENCE_TYPE[NSTAGE]="053";
  get_header();
  next;
}
/^B053F04/ { 
  SEQUENCE_NUMBER[NSTAGE]=$5; 
  next;
}
/^B053F05/ { 
  IN_UNITS[NSTAGE]=$6; 
  ALLIN[NSTAGE]=all_after(6);
  INPUTUNITS=$6;
  next; 
}
/^B053F06/ { 
  OUT_UNITS[NSTAGE]=$6; 
  ALLOUT[NSTAGE]=all_after(6); 
  OUTPUTUNITS=$6;
  next; 
}
/^B053F07/ { NORM_FACTOR[NSTAGE]=$5; next; }
/^B053F08/ { NORM_FREQUENCY[NSTAGE]=$4; next; }
/^B053F09/ { NZEROES[NSTAGE]=$5; next; }
/^B053F14/ { NPOLES[NSTAGE]=$5; next; }
/^B053F10-13/ { I=$2; RZERO[NSTAGE,I]=$3; IZERO[NSTAGE,I]=$4; next; }
/^B053F15-18/ { I=$2; RPOLE[NSTAGE,I]=$3; IPOLE[NSTAGE,I]=$4; next; }
/^B053F/ { warning_unprocessed_field(53); next; }
# ============================================================================
# type 054 - response (coefficients)
/^B054F03/ {
  NSTAGE++;
  TRANSFER_TYPE[NSTAGE]=$5; 
  SEQUENCE_TYPE[NSTAGE]="054";
  get_header();
  next;
}
/^B054F04/ { 
  SEQUENCE_NUMBER[NSTAGE]=$5; 
  next;
}
/^B054F05/ { 
  IN_UNITS[NSTAGE]=$6; 
  ALLIN[NSTAGE]=all_after(6);
  INPUTUNITS=$6;
  next; 
}
/^B054F06/ { 
  OUT_UNITS[NSTAGE]=$6; 
  ALLOUT[NSTAGE]=all_after(6); 
  OUTPUTUNITS=$6;
  next; 
}
/^B054F07/ { NNUMERATORS[NSTAGE]=$5; next; }
/^B054F08-09/ { I=$2; COEFF[NSTAGE,I]=$3; next; }
/^B054F10/ { NDENOMINATORS[NSTAGE]=$5; next; }
/^B054F/ { warning_unprocessed_field(54); next; }
# ============================================================================
# type 055 - response (list)
/^B055F/ { warning_unprocessed_blockette(55); next; }
# ============================================================================
# type 056 - generic response 
/^B056F/ { warning_unprocessed_blockette(56); next; }
# ============================================================================
# type 057 - decimation
/^B057F03/ {
  NSTAGE++;
  SEQUENCE_NUMBER[NSTAGE]=$5; 
  SEQUENCE_TYPE[NSTAGE]="057";
  get_header();
  next;
}
/^B057F04/ { INPUT_RATE[NSTAGE]=$5; next; }
/^B057F05/ { DECIMATION_FACTOR[NSTAGE]=$4; next; }
/^B057F06/ { DECIMATION_OFFSET[NSTAGE]=$4; next; }
/^B057F07/ { ESTIM_DELAY[NSTAGE]=$5; next; }
/^B057F08/ { APPLIED_CORR[NSTAGE]=$5; next; }
/^B057F/ { warning_unprocessed_blockette(57); next; }
# ============================================================================
# type 058 - channel identifier
/^B058F03/ { 
  NSTAGE++;
  SEQUENCE_NUMBER[NSTAGE]=$5; 
  SEQUENCE_TYPE[NSTAGE]="058";
  IN_UNITS[NSTAGE]=INPUTUNITS;
  OUT_UNITS[NSTAGE]=OUTPUTUNITS;
  get_header();
  next;
}
/^B058F04/ { SENSITIVITY[NSTAGE]=$3; next; }
/^B058F05/ { 
  FREQUENCY[NSTAGE]=$5; 
  FREQUENCY_UNITS[NSTAGE]=$6; 
  next;
  }
/^B058F06/ { NUMCAL[NSTAGE]=$5; next; }
/^B058F/ { warning_unprocessed_field(58); next; }
# ============================================================================
# type 059 - channel comment
/^B059F/ { warning_unprocessed_blockette(59); next; }
# ============================================================================
# type 060 - response reference
/^B060F/ { warning_unprocessed_blockette(60); next; }
# ============================================================================
# type 061 - FIR response
/^B061F/ { warning_unprocessed_blockette(61); next; }
# ============================================================================
/^#/ { next; }
{ 
  printf("WARNING: %-12s unprocessed line (record #%d):\n", 
    $1, FNR);
  print;
} 
# ============================================================================
END {
  printf ("%s\n", "$Id$");
  printf ("Found %d stage descriptions in response file %s\n", 
    NSTAGE, FILENAME);
# cycle through all recorded stages
  for (ISTAGE=0; ISTAGE<NSTAGE; ++ISTAGE)
  {
    I=ISTAGE+1;
# check if stages matches selection
    if (match_station(STATION_CODE[I], CHANNEL_NAME[I]))
    {
      printf ("\nValues for stage %d (sequence number %d):\n", 
        I, SEQUENCE_NUMBER[I]);
      if ( SEQUENCE_NUMBER[I] < 2 ) { print_header(I); }
# Blockette 53
# ============
      if (SEQUENCE_TYPE[I] == "053") 
      {
        printf ("053 Response (Poles & Zeros) Blockette\n");
        printf ("This stage specifies poles and zeroes\n");
        printf ("              input units: %s\n", ALLIN[I]);
        printf ("             output units: %s\n", ALLOUT[I]);
        printf ("     normalization factor: %15.7g\n", NORM_FACTOR[I]);
        printf ("  normalization frequency: %15.7g\n", NORM_FREQUENCY[I]);
        printf ("    type of specification: %s\n", TRANSFER_ALLTYPE[I]);
        printf ("%d zeroes:\n", NZEROES[I]);
        for (k=0; k<NZEROES[I]; ++k)
        {
          printf ("  %10.4g + i %10.4g\n", RZERO[I,k], IZERO[I,k]);
        }
        printf ("%d poles:\n", NPOLES[I]);
        for (k=0; k<NPOLES[I]; ++k)
        {
          printf ("  %10.4g + i %10.4g\n", RPOLE[I,k], IPOLE[I,k]);
        }
        printf ("%d lowpass poles\n", NPOLES[I]-NZEROES[I]);
        printf ("%d highpass poles\n", NZEROES[I]);
        type=TRANSFER_TYPE[I];
# process all zeroes
        k=0;
        while ( k<NZEROES[I] )
        {
          l=k+1;
          ord=1;
          if ( l < NZEROES[I] )
          {
            if (complexpair(RZERO[I,k],IZERO[I,k],
                            RZERO[I,l],IZERO[I,l], type))
            { ord=2; }
          }
          if (ord == 2)
          {
            printf ("second order zero at %10.4g Hz (%10.4g s)",
              frequency(RZERO[I,k],IZERO[I,k],type), 
              period(RZERO[I,k],IZERO[I,k],type));
            printf (" with damping %10.4g of critical\n",
              damping(RZERO[I,k],IZERO[I,k],type));
            ++k;
          }
          else
          {
            printf (" first order zero at %10.4g Hz (%10.4g s)\n",
              frequency(RZERO[I,k],IZERO[I,k],type), 
              period(RZERO[I,k],IZERO[I,k],type));
          }
          ++k;
        }
# process all poles
        k=0;
        while ( k<NPOLES[I] )
        {
          l=k+1;
          ord=1;
          if ( l < NPOLES[I] )
          {
            if (complexpair(RPOLE[I,k],IPOLE[I,k],
                            RPOLE[I,l],IPOLE[I,l], type))
            { ord=2; }
          }
          if (ord == 2)
          {
            printf ("second order pole at %10.4g Hz (%10.4g s)",
              frequency(RPOLE[I,k],IPOLE[I,k],type), 
              period(RPOLE[I,k],IPOLE[I,k],type));
            printf (", damping is %6.4f of crit.\n",
              damping(RPOLE[I,k],IPOLE[I,k],type));
            ++k;
          }
          else
          {
            printf (" first order pole at %10.4g Hz (%10.4g s)\n",
              frequency(RPOLE[I,k],IPOLE[I,k],type), 
              period(RPOLE[I,k],IPOLE[I,k],type));
          }
          ++k;
        }
      }
# Blockette 54
# ============
      else if (SEQUENCE_TYPE[I] == "054") 
      {
        printf ("054 Response (Coefficients) Blockette\n");
        printf ("This stage specifies polynomial coefficients\n");
        printf ("              input units: %s\n", ALLIN[I]);
        printf ("             output units: %s\n", ALLOUT[I]);
        printf ("    type of specification: %s\n", TRANSFER_ALLTYPE[I]);
        printf ("%d numerator coefficients:\n", NNUMERATORS[I]);
        if (NNUMERATORS[I]>0)
        {
          if (SELECT_COEFF_OUTPUT)
          {
            for (k=0; k<NNUMERATORS[I]; ++k)
            {
              printf("  #%3.3d: %15.8e\n", k, COEFF[I, k]);
            }
          }
          else
          {
            printf("Numerator coefficients are available.\n");
            printf("Please use PRINTCOEFF to select printing of coefficients.\n");
          }
        }
        printf ("%d denominator coefficients:\n", NDENOMINATORS[I]);
        if (NDENOMINATORS[I]>0)
        {
          printf("WARNING: not yet impleneted\n");
        }
      }
# Blockette 57
# ============
      else if (SEQUENCE_TYPE[I] == "057") 
      {
        printf ("057 Decimation Blockette\n");
        printf ("This stage specifies data stream decimation\n");
        print_value("Input sample rate", INPUT_RATE[I]);
        print_number("Decimation factor", DECIMATION_FACTOR[I]);
        print_number("Decimation offset", DECIMATION_OFFSET[I]);
        print_value_w_units("Estimated delay", ESTIM_DELAY[I], "s");
        print_value_w_units("Applied correction", APPLIED_CORR[I], "s");
      }
# Blockette 58
# ============
      else if (SEQUENCE_TYPE[I] == "058") 
      {
        printf ("058 Channel Sensitivity/Gain Blockette\n");
        printf ("This stage specifies a sensitivity\n");
        printf ("The sensitivity of this channel is\n");
        printf ("  %15.7g (%s)/(%s)  at %10.4g %s\n",
          SENSITIVITY[I], OUT_UNITS[I], IN_UNITS[I],
          FREQUENCY[I], FREQUENCY_UNITS[I]);
        printf ("Reciprocal of gain:  %15.7g (%s)/(%s)\n",
          1./SENSITIVITY[I], IN_UNITS[I], OUT_UNITS[I]);
        printf ("Number of calibrations: %d\n", NUMCAL[I]);
      }
    }
# ==============================
# stage does not match selection
# ==============================
    else
    {
      printf ("Skip stage %d (does not match selection)\n", I);
    }
  }
}
# ----- END OF SEEDresponse.awk ----- 
