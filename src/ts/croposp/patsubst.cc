/*! \file patsubst.cc
 * \brief provide pattern substitution as required in croposp (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 06/02/2019
 * 
 * provide pattern substitution as required in croposp (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 06/02/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define TF_PATSUBST_CC_VERSION \
  "TF_PATSUBST_CC   V1.0"

#include "croposp.h"
#include<tfxx/stringfunc.h>

namespace croposp {

  std::string patsubst(const std::string& templatestring,
                       const std::string& pattern,
                       const std::string& content)
  {
    std::string retval;
    retval=tfxx::string::patsubst(templatestring, pattern,
                                  tfxx::string::trimws(content));
    return(retval);
  } // std::string patsubst(const std::string& templatestring,
  //                      const std::string& pattern,
  //                      const std::string& content)

} // namespace croposp

/* ----- END OF patsubst.cc ----- */
