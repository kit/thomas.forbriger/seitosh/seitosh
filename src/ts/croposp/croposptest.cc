/*! \file croposptest.cc
 * \brief a program just to test components of croposp
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 07/02/2019
 * 
 * a program just to test components of croposp
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * REVISIONS and CHANGES 
 *  - 07/02/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define CROPOSPTEST_VERSION \
  "CROPOSPTEST   V1.0   a program just to test components of croposp"

#include "croposp.h"
#include <iostream>
#include <tfxx/commandline.h>
#include <tfxx/misc.h>

using std::cout;
using std::cerr;
using std::endl;

struct Options {
  bool pairs, verbose, triples;
  unsigned int npairs, ntriples;
}; // struct Options

int main(int iargc, char* argv[])
{

  // define usage information
  char usage_text[]=
  {
    CROPOSPTEST_VERSION "\n"
    "usage: croposptest [-v] [-pairs n] [-triples n]" "\n"
    "   or: croposptest --help|-h" "\n"
  };

  // define full help text
  char help_text[]=
  {
    " "
  };

  // define commandline options
  using namespace tfxx::cmdline;
  static Declare options[]= 
  {
    // 0: print help
    {"help",arg_no,"-"},
    // 1: verbose mode
    {"v",arg_no,"-"},
    // 2: test pairs
    {"pairs",arg_yes,"4"},
    // 3: test pairs
    {"triples",arg_yes,"4"},
    {NULL}
  };

  // no arguments? print usage...
  if (iargc<2) 
  {
    cerr << usage_text << endl;
    exit(0);
  }

  // collect options from commandline
  Commandline cmdline(iargc, argv, options);

  // help requested? print full help text...
  if (cmdline.optset(0))
  {
    cerr << usage_text << endl;
    cerr << help_text << endl;
    exit(0);
  }

  Options opt;
  opt.verbose=cmdline.optset(1);
  opt.pairs=cmdline.optset(2);
  opt.npairs=cmdline.int_arg(2);
  opt.triples=cmdline.optset(3);
  opt.ntriples=cmdline.int_arg(3);

// ----------------------------------------------------------------------

  if (opt.pairs)
  {
    croposp::Pairs pairs(opt.npairs);
    cout << TFXX_value(opt.npairs) << " "
      << TFXX_value(pairs.size()) << " "
      << TFXX_value(pairs.pairs()) << endl;

    cout << endl << "unique combinations (in order)" << endl;
    for (unsigned int k=1; k<opt.npairs; ++k)
    {
      for (unsigned int j=0; j<k; ++j)
      {
        cout << TFXX_value(k) << " " << TFXX_value(j) << " "
          << TFXX_value(pairs(k,j)) << endl;
      }
    }

    cout << endl << "all combinations" << endl;
    for (unsigned int k=0; k<opt.npairs; ++k)
    {
      for (unsigned int j=0; j<opt.npairs; ++j)
      {
        if (k!=j)
        {
        cout << TFXX_value(k) << " " << TFXX_value(j) << " "
          << TFXX_value(pairs(k,j)) << " "
          << TFXX_value(pairs(j,k));
        if (pairs.swap(k,j))
        {
          cout << " swap";
        }
        cout << endl;
        }
      }
    }
  } // if opt.pairs

// ----------------------------------------------------------------------

  if (opt.triples)
  {
    croposp::Triples triples_pf(opt.ntriples, true);
    croposp::Triples triples_sf(opt.ntriples, false);
    cout << TFXX_value(opt.ntriples) << " "
      << TFXX_value(triples_pf.size()) << " "
      << TFXX_value(triples_pf.pairs()) << " "
      << TFXX_value(triples_pf.triples()) << endl;
    cout << TFXX_value(opt.ntriples) << " "
      << TFXX_value(triples_sf.size()) << " "
      << TFXX_value(triples_sf.pairs()) << " "
      << TFXX_value(triples_sf.triples()) << endl;

    cout << endl << "single cycles fast:" << endl;
    for (unsigned int k=1; k<opt.ntriples; ++k)
    {
      for (unsigned int l=0; l<k; ++l)
      {
        for (unsigned int i=0; i<opt.ntriples; ++i)
        {
          if ((k!=i) && (l!=i))
          {
            cout << TFXX_value(i) << " " 
              << TFXX_value(k) << " "
              << TFXX_value(l) << " "
              << TFXX_value(triples_sf(i,k,l)) << endl;
          }
        }
      }
    }

    cout << endl << "pairs cycle fast:" << endl;
    for (unsigned int i=0; i<opt.ntriples; ++i)
    {
      for (unsigned int k=1; k<opt.ntriples; ++k)
      {
        for (unsigned int l=0; l<k; ++l)
        {
          if ((k!=i) && (l!=i))
          {
            cout << TFXX_value(i) << " " 
              << TFXX_value(k) << " "
              << TFXX_value(l) << " "
              << TFXX_value(triples_pf(i,k,l)) << endl;
          }
        }
      }
    }
  } // if (opt.triples)

}

/* ----- END OF croposptest.cc ----- */
