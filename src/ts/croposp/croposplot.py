#!/usr/bin/env python
# this is <croposplot.py>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
# 
# create graphical diagram from output of croposp
#
# ----
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
# ----
#
# REVISIONS and CHANGES 
#    31/01/2019   V1.0   Thomas Forbriger
#    14/02/2019   V1.1   first version providing reasonable functionality
#    01/03/2019   V1.3   support portrait orientation
#    15/04/2019   V1.3a  adjust list of colors to improve contrast
#    13/08/2024   V1.3b  python syntax fixes
# 
# ============================================================================
__version__ = "croposplot 2024-08-13 V1.3b"
__author__ = "Thomas Forbriger"
# ----------------------------------------------------------------------------
# backend
# -------
# For screen-plots an appropriate backend must be chosen. This should be done
# by an entry in ~/.config/matplotlib/matplotlibrc like
# backend : qt5agg
# 
# Alternatively the first two statements may be set to:
#
#   import matplotlib
#    matplotlib.use('qt5agg')
#
# However, the backend then would be hardcoded, which is not desired.
#
# ----------------------------------------------------------------------------

import sys
import os
import getopt
import string
import re
import numpy as np
import matplotlib.pyplot as plt

# ============================================================================
def usage():
  """
  print usage information
  """
  print(__version__)
  print('Usage: '+sys.argv[0]+' [-v]')
  print('       [-o file] [-g|--grid] [--nologlog] [--nologx] [--nology]')
  print('       [--legfontsize s] [--legpos p] [--title t]')
  print('       [--xlabel l] [--ylabel l] [--titfontsize t]')
  print('       [--xlim l] [--ylim l] [--portrait]')
  print('       [--usemarkers] [--match re]')
  print('       file [file [file ...]]')
  print('   or: '+sys.argv[0]+' --help|-h')


# ----------------------------------------------------------------------------
def help():
  """
  print online help
  """
  usage()
  print(
  """
  Read multicolumn output files of spectral values as written by croposp and
  create graphical diagrams.

  file ...        multicolumn spectral data output of croposp
  -v              be verbose
  -o file         write to file
  --xlabel l      set label "l" on x-axis
  --ylabel l      set label "l" on y-axis
  -g|--grid       plot grid
  --portrait      use protrait orientation (default: landscape)
  --nologlog      plot on linear scales
  --nologx        use linear scale for x-axis
  --nology        use linear scale for y-axis
  --usemarkers    use graph markers
  --legfontsize s set font size for legend
  --legpos p      set position of legend
  --title t       set plot title
  --titfontsize s set font size for legend
  --xlim min:max  set x-axis limits
  --ylim min:max  set y-axis limits
  --match re      only display curve for which the label matches the regular
                  expression "re"
  """)

# -----------------------------------------------------------------------------
class Error(Exception):

  def __init__(self, msg=""):
    self.msg = str(msg)

  def display(self):
    sys.stderr.write("croposplot ERROR: " + self.msg + "\n")

# ============================================================================

def main(argv=None):
  """
  main body of program
  evaluate command line options and control processing
  """
  if argv is None:
    argv=sys.argv

  try:
    opts, args=getopt.getopt(sys.argv[1:], 'hvo:g', ['help', 'nologlog',
    'nologx', 'nology', 'grid', 'xlabel=', 'legfontsize=', 'title=',
    'legpos=', 'titfontsize=', 'ylabel=', 'xlim=', 'ylim=',
    'usemarkers', 'match=', 'portrait'])
  except getopt.GetoptError as err:
    print(err.msg)
    exit(0)

  verbose=False
  global DEBUG
  DEBUG=False
  outfile='x11'
  nologlog = False
  nologx = False
  nology = False
  grid = False
  xlabel = 'frequency / Hz'
  ylabel = None
  opt_legendfontsize='xx-small'
  opt_titlefontsize='large'
  opt_title = None
  opt_legposition = 'best'
  opt_xlim = None
  opt_ylim = None
  opt_usemarkers = False
  opt_match = None
  opt_orientation='landscape'

  for (opt, arg) in opts:
    if opt == '-v':
      verbose=True
    elif opt == '-D':
      DEBUG=True
    elif opt == '-o':
      outfile=arg
    elif opt in ("--xlim"):
      try:
        opt_xlim = [float(x) for x in arg.split(":")]
        if 2 != len(opt_xlim):
          raise
      except:
        raise Error("Invalid 'xlim' argument.")
    elif opt in ("--ylim"):
      try:
        opt_ylim = [float(y) for y in arg.split(":")]
        if 2 != len(opt_ylim):
          raise
      except:
        raise Error("Invalid 'ylim' argument.")
    elif opt in ('--match'):
      opt_match=arg
    elif opt in ('--xlabel'):
      xlabel=arg
    elif opt in ('--ylabel'):
      ylabel=arg
    elif opt in ('--legfontsize'):
      opt_legendfontsize=arg
    elif opt in ('--titfontsize'):
      opt_titlefontsize=arg
    elif opt in ('--legpos'):
      opt_legposition=arg
    elif opt in ('--title'):
      opt_title=arg
    elif opt in ("-g", "--grid"):
      grid = True
    elif opt in ("--portrait"):
      opt_orientation = 'portrait'
    elif opt in ("--nologlog"):
      nologlog = True
    elif opt in ("--nologx"):
      nologx = True
    elif opt in ("--nology"):
      nology = True
    elif opt in ("--usemarkers"):
      opt_usemarkers = True
    elif (opt == '-h') or (opt == '--help'):
      help()
      exit(0)
    else :
      usage()
      exit(0)

  if DEBUG:
    print(opts)
    print(args)

  if len(args) < 1 and len(opts) < 1:
    usage()
    exit(0)

  print(__version__)
  print("Read croposp output files and create diagram")

  Figure=plt.figure()
  if opt_orientation == 'portrait':
    Figure.set_figheight(10.)
    Figure.set_figwidth(7.5)

  plt.grid(grid)
  # using logscale if desired
  if nologx and nology:
    nologlog = True

  if nology and not nologlog:
    if verbose:
      sys.stdout.write("specplot: Using log scaling for x-axis ... \n")
    plt.semilogx()
  elif nologx and not nologlog:
    if verbose:
      sys.stdout.write("specplot: Using log scaling for y-axis ... \n")
    plt.semilogy()
  elif not nologlog:
    if verbose:
      sys.stdout.write("specplot: Using loglog scaling  ... \n")
    plt.loglog()
  else:
    if verbose:
      sys.stdout.write("specplot: Disable logscale ... \n")

  plt.xlabel(xlabel)
  if ylabel is not None:
    plt.ylabel(ylabel)
  if opt_xlim is not None:
    plt.xlim(opt_xlim[0], opt_xlim[1]) 
  if opt_ylim is not None:
    plt.ylim(opt_ylim[0], opt_ylim[1]) 

# support curve style cycling
  mymarkers=['o','*','p','v','<','>','d','D','.']
  mycolors=['#000000',
      '#0000ff',
      '#ff0000',
      '#ff00ff',
      '#22ffff',
      '#007777',
      '#ffff22',
      '#880000',
      '#997700',
      '#555555',
      '#999999']
  icurve=0

  for specfile in args:
    if verbose:
      print('read file %s' % specfile)
# read trace labels
    labellines=list(filter((lambda x: re.match('^# #',x)), 
      open(specfile).readlines()))
# read trace data
    data=np.loadtxt(specfile, unpack=False)
    for i in range(1,len(data[0,:])):
      label=re.sub('^# #\d+: ', '', labellines[i-1].strip())
      match=True
      if opt_match is not None:
        match=(re.search(opt_match, label) is not None)
      if match:
        if verbose:
          print('plot curve %d: %s' % (i, label))
        themarker=None
        if opt_usemarkers:
          themarker=mymarkers[icurve % len(mymarkers)]
        thecolor=mycolors[icurve % len(mycolors)]
        plt.plot(data[:,0], data[:,i], label=label,
            color=thecolor, marker=themarker)
        plt.legend(fontsize=opt_legendfontsize, loc=opt_legposition)
        icurve=1+icurve

    if opt_title is not None:
      plt.title(re.sub('\\\\n','\n',opt_title), fontsize=opt_titlefontsize)

  if outfile=='x11':
    plt.show(True)
  else:
    plt.savefig(outfile, orientation=opt_orientation)

  return 0

# ============================================================================
if __name__ == '__main__':
  sys.exit(main())

# ----- END OF croposplot.py ----- 
