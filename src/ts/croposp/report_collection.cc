/*! \file report_collection.cc
 * \brief report a collection of time series (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 06/02/2019
 * 
 * report a collection of time series (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 06/02/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define TF_REPORT_COLLECTION_CC_VERSION \
  "TF_REPORT_COLLECTION_CC   V1.0"

#include "croposp.h"
#include<iostream>
#include<tfxx/misc.h>

using std::cout;
using std::endl;

namespace croposp {

  void report_collection(const TCollection& collection,
                         const TMetaVector& metadata,
                         const bool& debug)
  {
    TFXX_assert(collection.size() == metadata.size(),
                "data inconsitency; report this as a program bug");
    TCollection::const_iterator i_series=collection.begin();
    TMetaVector::const_iterator i_meta=metadata.begin();
    while (i_series != collection.end() &&
           i_meta != metadata.end())
    {
      cout << "  "
        << i_series->header.station << " "
        << i_series->header.channel << " "
        << i_series->header.auxid << " "
        << "dt=" << i_series->header.dt << "s "
        << "begin: " << i_series->header.date.timestring() << " "
        << "n: " << i_series->header.nsamples 
        << endl;
      cout << "  " << i_meta->label << endl;
      TFXX_debug(debug, "report_collection",
                 TFXX_value(i_series->f())
                 << " " <<
                 TFXX_value(i_series->l())
                 << " " <<
                 TFXX_value(i_series->size()));
      ++i_series;
      ++i_meta;
    }
  } // void report_collection(const Tcollection& collection)

} // namespace croposp

/* ----- END OF report_collection.cc ----- */
