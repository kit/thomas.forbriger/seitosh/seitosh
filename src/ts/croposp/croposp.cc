/*! \file croposp.cc
 * \brief Cross power spectral density
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 25/12/2018
 * 
 * Cross power spectral density
 * 
 * Copyright (c) 2018 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 25/12/2018   V1.0   Thomas Forbriger
 *  - 14/02/2019   V1.1   first operational version (coarsely tested)
 *  - 03/03/2019   V1.2   compute magnitude squared coherence rather than
 *                        its suqare root
 *                        commonly in literature coherence refers to the
 *                        magnitude squared coherence
 *  - 12/04/2019   V1.3   bux fixes:
 *                        PSD is not required for transfer of phase and the
 *                        existence of these value shall not be checked in
 *                        these cases; check for frequency array being smaller
 *                        than 2 (rather than 1); add verbosity output
 *  - 24/06/2020   V1.4   report default arguments
 *  - 02/10/2020   V1.5   specify PSD to be one-sided PSD
 * 
 * ============================================================================
 */
/*
 * version string is set in croposp.h
 *
#define CROPOSP_VERSION \
  "CROPOSP   V1.5   Cross power spectral density"
  */

#include "croposp.h"
#include <iostream>
#include <tsioxx/cmdlinefiles.h>
#include <tfxx/misc.h>
#include <tfxx/seitosh.h>
#include <aff/seriesoperators.h>

using std::cout;
using std::cerr;
using std::endl;

/* ====================================================================== */
  
// define usage information
char reference_sleeman_et_al_2006[]=
{
  "Reinoud Sleeman, Arie van Wettum, and Jeannot Trampert, 2006.\n"
  "  Three-Channel Correlation Analysis: A New Technique to Measure\n"
  "  Instrumental Noise of Digitizers and Seismic Sensors.\n"
  "  Bull. Seism. Soc. Am., Vol. 96, No. 1, pp. 258–271.\n"
  "  doi: 10.1785/0120050032\n"
  "  http://www.geo.uu.nl/~seismain/pdf/bssa06-inst.pdf (accessed 2019-01-25)\n" };

/* ---------------------------------------------------------------------- */
// report default value of command line argument
void reportdefault(std::ostream& os, const tfxx::cmdline::Declare& o)
{
  os << "default argument to option \'-" << o.opt_string << "\':";
  os << " \'" << o.arg_default << "\'" << endl;
} // void reportdefault(std::ostream& os, const tfxx::cmdline::Declare& o)

/* ====================================================================== */

int main(int iargc, char* argv[])
{

  // define usage information
  char usage_text[]=
  {
    CROPOSP_VERSION "\n"
    "usage: croposp [-verbose] [-itype f] [-trim] [-datetolerance t]" "\n"
    "               [-pattern p] [-overwrite] [-DEBUG]" "\n"
    "               [-log n] [-avgout] [-avgcomplex]\n"
    "               [-nsegments n] [-divisor n] [-padfactor n] [-overlap f]\n"
    "               [-psd f] [-transfer f] [-phase f] [-coherence f]" "\n"
    "               [-npsd f] [-npsdmode f]\n"
    "               [-prefixpsd s] [-prefixnpsd s] [-prefixtransfer s]\n"
    "               [-prefixphase s] [-prefixcoherence s]" "\n"
    "       file [t:sel] [f:format] [n:label] [file [t:s] [f:f] [n:l]] [...]\n"
    "   or: croposp --help|-h" "\n"
  };

  // define full help text
  char help_text[]=
  {
    "-verbose           be verbose\n"
    "-DEBUG             produce debugging output\n"
    "-itype f           set default input file format (this value can\n"
    "                   be overwritten by the format key at a file name)\n"
    "-trim              trim time series to identical begin and length\n"
    "-datetolerance t   set tolerance as a fraction of sampling interval\n"
    "                   when comparing time series header date values\n"
    "-pattern p         set pattern for signal label\n"
    "                   %C: channel (WID2 header field)\n"
    "                   %S: station (WID2 header field)\n"
    "                   %I: instype (WID2 header field)\n"
    "                   %A: auxid (WID2 header field)\n"
    "                   %N: label set for file name\n"
    "                   %F: file name\n"
    "                   %NT: number of trace in file\n"
    "-patpsd s          set label pattern for psd output to \"s\"\n"
    "                   %1: signal label\n"
    "-patnpsd s         set label pattern for incoherent psd output to \"s\"\n"
    "                   %1: label of signal\n"
    "                   %2: label of first reference signal\n"
    "                   %3: label of second reference signal\n"
    "                   %M: label of npsd mode\n"
    "-pattransfer s     set label pattern for amplitude ratio output to \"s\"\n"
    "                   %1: label of first signal\n"
    "                   %2: label of second signal\n"
    "                   %3: label of reference signal\n"
    "-patphase s        set label pattern for phase delay output to \"s\"\n"
    "                   %1: label of first signal\n"
    "                   %2: label of second signal\n"
    "                   %3: label of reference signal\n"
    "-patcoherence s    set label pattern for coherence output to \"s\"\n"
    "                   %1: label of first signal\n"
    "                   %2: label of second signal\n"
    "\n"
    "computation options:\n"
    "-log n       map averages to logarithmic scale with \"n\"\n"
    "             samples pre decade\n"
    "-avgout      do mapping on logarithmic scale not for values\n"
    "             of raw PSD and cross-PSD but for final results of\n"
    "             computation; in cases where computation is based on\n"
    "             cross power spectra density and where the differential\n"
    "             phase response of input channels strongly varies with\n"
    "             frequency, results may differ; using this option\n"
    "             increases computation time\n"
    "-avgcomplex  only effective together with -avgout\n"
    "             if computing coherence results, average the complex\n"
    "             fraction of cross-power spectral density values\n"
    "             along frequency, before taking the absolute value, etc\n"
    "-nsegments n set number of segments to be used for each input signal\n"
    "-divisor n   set number of samples to an integer multiple of \"n\"\n"
    "-padfactor n pad with zeroes to increase length of series by a factor\n"
    "             of \"n\"\n"
    "-overlap f   let segments overlap by a fraction of \"f\"\n"
    "-npsdmode m  select npsd mode (see below)\n"
    "\n"
    "output options:\n"
    "-overwrite    overwrite existing output files\n"
    "-psd f        compute power spectral density and write to file \"f\"\n"
    "-npsd f       compute power spectral density of incoherent component\n"
    "              and write to file \"f\"\n"
    "              Sleeman et al (2006, eq. 14)\n"
    "-transfer f   compute amplitude transfer function for pairs of\n"
    "              signals and write to file \"f\"\n"
    "              Sleeman et al (2006, eq. 13)\n"
    "-phase f      compute phase transfer function for pairs of\n"
    "              signals and write to file \"f\"\n"
    "              Sleeman et al (2006, eq. 13)\n"
    "-coherence f  compute magnitude squared coherecy for pairs of signals\n"
    "              and write to file \"f\"\n"
    "\n"
    "keys to be appended to file names if required:\n"
    "t:sel        trace selection\n"
    "f:format     file format\n"
    "n:label      label to be used in plot legend\n"
    "\n"
    "Computed PSD values are one-sided PSD, i.e. twice the Fourier\n"
    "transform of the normalized auto-correlation function, which is\n"
    "commonly called power spectral density in signal analysis theory.\n"
  };

  // define commandline options
  using namespace tfxx::cmdline;
  static Declare options[]= 
  {
    // 0: print help
    {"help",arg_no,"-"},
    // 1: verbose mode
    {"verbose",arg_no,"-"},
    // 2: default input file format
    {"itype",arg_yes,"sff"},
    // 3: trim time series
    {"trim",arg_no,"-"},
    // 4: default input file format
    {"datetolerance",arg_yes,"0."},
    // 5: debug mode
    {"DEBUG",arg_no,"-"},
    // 6: pattern for trace label
    {"pattern",arg_yes,"%S:%C:%A:%I"},
    // 7: compute PSD
    {"psd",arg_yes,"-"},
    // 8: compute PSD of incoherent component
    {"npsd",arg_yes,"-"},
    // 9: compute transfer function
    {"transfer",arg_yes,"-"},
    // 10: compute coherence
    {"coherence",arg_yes,"-"},
    // 11: map to logarithmic frequency sampling
    {"log",arg_yes,"1"},
    // 12: overwrite existing output files
    {"overwrite",arg_no,"-"},
    // 13: set number of segments
    {"nsegments",arg_yes,"1"},
    // 14: set number of divisor for selection of number of samples
    {"divisor",arg_yes,"1"},
    // 15: set padding factor
    {"padfactor",arg_yes,"1"},
    // 16: set overlap fraction
    {"overlap",arg_yes,"0.5"},
    // 17: compute phase delay
    {"phase",arg_yes,"-"},
    // 18: set psd label pattern
    {"patpsd",arg_yes,"PSD %1"},
    // 19: set npsd label pattern
    {"patnpsd",arg_yes,"NPSD %1 (ref: %2 %3) %M"},
    // 20: set transfer label pattern
    {"pattransfer",arg_yes,"AMP %1/%2 (ref: %3)"},
    // 21: set phase label pattern
    {"patphase",arg_yes,"PHA %1-%2 (ref: %3)"},
    // 22: set coherence label pattern
    {"patcoherence",arg_yes,"MSC %1 %2"},
    // 23: average upon output
    {"avgout",arg_no,"-"},
    // 24: average upon output
    {"npsdmode",arg_yes,"0"},
    // 25: average complex value of fraction of cross-power spectral density
    {"avgcomplex",arg_no,"-"},
    {NULL}
  };

  // define command line keys for input files
  static const char tracekey[]="t";
  static const char formatkey[]="f";
  static const char labelkey[]="n";

  // define commandline argument modifier keys
  static const char* cmdlinekeys[]={tracekey, formatkey, labelkey, 0};

  // nspd mode strings
  static const char npsd_label_abs[]="|Pkk-(Plk*Pkj/Plj)|";
  static const char npsd_label_abs2[]="|Pkk-|Plk*Pkj/Plj||";
  static const char npsd_label_real[]="|Pkk-real(Plk*Pkj/Plj)|";
  static const char npsd_label_imag[]="|imag(Plk*Pkj/Plj)|";
  static const char* npsd_mode_label[]
    ={npsd_label_abs, npsd_label_abs2, npsd_label_real, npsd_label_imag, 0};

  /* ====================================================================== */
  /* analyze command line
   * --------------------
   */

  // no arguments? print usage...
  if (iargc<2) 
  {
    cerr << usage_text << endl;
    cerr << tfxx::seitosh::repository_reference << endl;
    exit(0);
  }

  // collect options from commandline
  Commandline cmdline(iargc, argv, options);

  // help requested? print full help text...
  if (cmdline.optset(0))
  {
    cerr << usage_text << endl;
    cerr << help_text << endl;
    cerr << endl;
    cerr << "npsd modes:" << endl;
    for (unsigned int l=0; l<4; ++l)
    {
      cerr << l << ": " << npsd_mode_label[l] << endl;
    }
    cerr << endl;
    reportdefault(cerr, options[24]);
    reportdefault(cerr, options[6]);
    reportdefault(cerr, options[18]);
    reportdefault(cerr, options[19]);
    reportdefault(cerr, options[20]);
    reportdefault(cerr, options[21]);
    reportdefault(cerr, options[22]);
    cerr << endl << endl;
    cerr << "References" << endl
      <<    "----------" << endl;
    cerr << reference_sleeman_et_al_2006 << endl;
    cerr << tfxx::seitosh::repository_reference << endl;
    exit(0);
  }

  croposp::Options opt;
  opt.verbose=cmdline.optset(1);
  opt.inputformat=cmdline.string_arg(2);
  opt.trim=cmdline.optset(3);
  opt.datetolerance=cmdline.double_arg(4);
  opt.debug=cmdline.optset(5);
  opt.labelpattern=cmdline.string_arg(6);

  opt.compute_psd=cmdline.optset(7);
  opt.outfile_psd=cmdline.string_arg(7);
  opt.compute_npsd=cmdline.optset(8);
  opt.outfile_npsd=cmdline.string_arg(8);
  opt.compute_transfer=cmdline.optset(9);
  opt.outfile_transfer=cmdline.string_arg(9);
  opt.compute_coherence=cmdline.optset(10);
  opt.outfile_coherence=cmdline.string_arg(10);

  opt.logscale=cmdline.optset(11);
  opt.n_per_decade=cmdline.int_arg(11);
  opt.overwrite=cmdline.optset(12);
  opt.nsegments=cmdline.int_arg(13);
  opt.divisor=cmdline.int_arg(14);
  opt.padfactor=cmdline.int_arg(15);
  opt.overlap=cmdline.double_arg(16);

  opt.compute_phase=cmdline.optset(17);
  opt.outfile_phase=cmdline.string_arg(17);

  opt.pattern_psd=cmdline.string_arg(18);
  opt.pattern_npsd=cmdline.string_arg(19);
  opt.pattern_transfer=cmdline.string_arg(20);
  opt.pattern_phase=cmdline.string_arg(21);
  opt.pattern_coherence=cmdline.string_arg(22);

  opt.avg_on_output=cmdline.optset(23);
  opt.npsd_mode=cmdline.int_arg(24);
  opt.avg_complex=cmdline.optset(25);

  TFXX_assert((opt.npsd_mode>=0) && (opt.npsd_mode<4),
              "npsd mode out of range");
  TFXX_assert(opt.n_per_decade>0,
              "number of samples per decade must be finite and positive");
  TFXX_assert(opt.nsegments>0,
              "number of segments must be finite and positive");
  TFXX_assert(opt.divisor>0,
              "divisor must be finite and positive");
  TFXX_assert(opt.padfactor>0,
              "padding factor must be finite and positive");

  TFXX_assert(((opt.overlap >= 0.) && (opt.overlap <=0.5)),
              "overlap fraction is out of accepted range");
  TFXX_assert(((opt.datetolerance >= 0.) && (opt.datetolerance <=1.)),
              "datetolerance is out of accepted range");
  if (opt.avg_complex)
  {
    TFXX_assert(opt.avg_on_output,
                "use -avgcomplex only together with -avgout");
  }
  
  // extract commandline arguments
  TFXX_assert(cmdline.extra(), "missing input file");
  tfxx::cmdline::Tparsed arguments=parse_cmdline(cmdline, cmdlinekeys);

  /* ====================================================================== */
  /* read data, prepare labels and trim time series if requested
   * -----------------------------------------------------------
   */

  // read data files
  if (opt.verbose) 
  {
    cout << "Read time series data" << endl;
  }
  ts::sff::TDFileList input_file_list;
  tfxx::cmdline::Tparsed::const_iterator file=arguments.begin();
  while (file != arguments.end())
  {
    std::string format=opt.inputformat;
    if (file->haskey(formatkey))
    {
      format=file->value(formatkey);
    }
    input_file_list.push_back(ts::sff::readDSFF(*file, opt.verbose, 
                                                tracekey, format));
    ++file;
  }

  // collect traces
  croposp::TCollection collection_of_series;
  croposp::TMetaVector vector_of_metadata;
  typedef ts::sff::DFile::Tfile Tfile;
  typedef Tfile::Ttracevector Ttracevector;
  ts::sff::TDFileList::const_iterator i_file=input_file_list.begin();
  while (i_file != input_file_list.end())
  {
    Ttracevector::const_iterator i_trace=i_file->data.begin();
    while (i_trace != i_file->data.end())
    {
      collection_of_series.push_back(*i_trace);
      croposp::Meta metadata;
      metadata.filearguments=i_file->arguments;
      metadata.label=opt.labelpattern;
      metadata.label=croposp::patsubst(metadata.label, "%C", 
                                       i_trace->header.wid2().channel);
      metadata.label=croposp::patsubst(metadata.label, "%S", 
                                       i_trace->header.wid2().station);
      metadata.label=croposp::patsubst(metadata.label, "%A", 
                                       i_trace->header.wid2().auxid);
      metadata.label=croposp::patsubst(metadata.label, "%I", 
                                       i_trace->header.wid2().instype);
      metadata.label=croposp::patsubst(metadata.label, "%F", 
                                       metadata.filearguments.name);
      std::ostringstream oss;
      oss << i_trace->traceindex();
      metadata.label=croposp::patsubst(metadata.label, "%NT", oss.str());
      if (metadata.filearguments.haskey(labelkey))
      {
        metadata.label=croposp::patsubst(metadata.label, "%N", 
                                         metadata.filearguments.value(labelkey));
      }
      vector_of_metadata.push_back(metadata);
      ++i_trace;
    }
    ++i_file;
  }

  // report traces
  if (opt.verbose)
  {
    cout << "Time series to be analyzed:" << endl;
    report_collection(collection_of_series,
                      vector_of_metadata);
  }

  // trim time series
  if (opt.trim)
  {
    TFXX_assert(collection_of_series.overlap(),
                "time series do not overlap");
    collection_of_series.trim_to_date();
    collection_of_series.trim_to_nsamples();

    // report traces
    if (opt.verbose)
    {
      cout << "Time series after trimming:" << endl;
      report_collection(collection_of_series,
                        vector_of_metadata, opt.debug);
    }
  }

  // check consistency
  ::sff::WID2compare comparer(::sff::Fdt | ::sff::Fnsamples | ::sff::Fdate);
  comparer.setdatetolerance(opt.datetolerance);
  TFXX_assert(collection_of_series.are_consistent(comparer),
              "time series are inconsistent");

  if (opt.compute_npsd || opt.compute_coherence)
  {
    if ((opt.nsegments < 2) && (!opt.logscale))
    {
      cout << "WARNING! The requested result requires averaging." << endl;
      cout << "         Current parameters avoid averaging of" << endl;
      cout << "         cross power spectral density. Results" << endl;
      cout << "         might be meaningless." << endl;
    }
  }

  /* ====================================================================== */

  // provide container for frequency values (will be filled upon computation
  // of PSD values)
  psd::TDseries frequencies;

  /* ---------------------------------------------------------------------- */
  /* compute power spectral density
   * ------------------------------
   */
  psd::DPSDComputer psd_computer;
  psd_computer.set_debug(opt.debug);
  psd_computer.set_verbose(opt.verbose);
  psd_computer.set_nsegments(opt.nsegments);
  psd_computer.set_divisor(opt.divisor);
  psd_computer.set_padfactor(opt.padfactor);
  psd_computer.set_overlap(opt.overlap);

  // provide container for PSD values
  croposp::TNamedSeriesVector PSD_vector;

  if (opt.compute_psd || opt.compute_npsd ||
      opt.compute_coherence)
  {
    if (opt.verbose)
    {
      cout << endl 
        << "Compute power spectral density for all signals." << endl;
      if (opt.logscale)
      {
        cout << "Map to logarithmic frequency scale";
        if (opt.avg_on_output)
        {
          cout << " upon output to file";
        }
        cout << "." << endl;
      }
    }

    croposp::TCollection::const_iterator i_series=collection_of_series.begin();
    croposp::TMetaVector::const_iterator i_meta=vector_of_metadata.begin();
    while (i_series != collection_of_series.end() &&
           i_meta != vector_of_metadata.end())
    {
      psd::TDISeries interval_series;
      interval_series.data=*i_series;
      interval_series.interval=i_series->header.dt;
      TFXX_debug(opt.debug, "croposp (compute PSD)",
                 TFXX_value(i_series->header.dt)
                 << " "
                 << TFXX_value(interval_series.interval));
      psd::TDISeries psd=psd_computer.psd(interval_series);

      if (i_series==collection_of_series.begin())
      {
        if (opt.logscale && !opt.avg_on_output)
        {
          if (opt.verbose)
          {
            cout << "Compute logarithmic frequency scale" << endl;
          }
          frequencies=psd::log_frequency(psd.interval, psd.data.size(),
                                         opt.n_per_decade);
        }
        else
        {
          if (opt.verbose)
          {
            cout << "Compute linear frequency scale" << endl;
          }
          frequencies=psd::lin_frequency(psd.interval, psd.data.size());
        } // if (opt.logscale && !opt.avg_on_output) ... else
      } // if (i_series==collection_of_series.begin())

      croposp::NamedSeries named_psd;
      if (opt.logscale && !opt.avg_on_output)
      {
        named_psd.series=psd::log_sampling(psd, frequencies);
      } // if (opt.logscale && !opt.avg_on_output)
      else
      {
        named_psd.series=psd.data;
      } // if (opt.logscale && !opt.avg_on_output) ... else

      named_psd.label=opt.pattern_psd;
      named_psd.label=croposp::patsubst(named_psd.label, "%1", 
                                        i_meta->label);
      PSD_vector.push_back(named_psd);

      if (opt.verbose)
      {
        cout << "  computed: " << named_psd.label << endl;
      }

      ++i_series;
      ++i_meta;
    } // while (i_series != collection_of_series.end() &&
      //        i_meta != vector_of_metadata.end())
    if (opt.compute_psd)
    {
      write_named_series(opt.outfile_psd,
                         "total power spectral density of signals",
                         frequencies, PSD_vector, opt);
    } // if (opt.compute_psd)
  } // if (opt.compute_psd || opt.compute_npsd || 
    //     opt.compute_coherence)

  /* ---------------------------------------------------------------------- */

  croposp::Pairs pairs(collection_of_series.size());

  // provide container for CPSD values
  croposp::TNamedCPSDVector CPSD_vector(pairs.pairs());

  if (opt.verbose)
  {
    cout << endl
      << "Provide storage for " << pairs.pairs() << " pairs "
      << "composed from " << pairs.size() << " signals."
      << endl
      << "CPSD_vector size is " << CPSD_vector.size()
      << endl;
  }

  if (opt.compute_transfer || opt.compute_phase || opt.compute_npsd ||
      opt.compute_coherence)
  {
    if (opt.verbose)
    {
      cout << endl 
        << "Compute cross power spectral density for all pairs of signals." 
        << endl;
      if (opt.logscale)
      {
        cout << "Map to logarithmic frequency scale";
        if (opt.avg_on_output)
        {
          cout << " upon output to file";
        }
        cout << "." << endl;
      }
    }

    TFXX_assert(collection_of_series.size()>1,
           "requested computation requires at least two input time series");

    for (unsigned int k=1; k<collection_of_series.size(); ++k)
    {
      for (unsigned int l=0; l<k; ++l)
      {
        TFXX_debug(opt.debug, "croposp",
                   "next pair " << TFXX_value(k) << " " << TFXX_value(l));

        psd::TDISeries::Tcoc isk, isl;
        isk.data=collection_of_series[k];
        isk.interval=collection_of_series[k].header.dt;
        isl.data=collection_of_series[l];
        isl.interval=collection_of_series[l].header.dt;

        psd::TDCISeries cpsd=psd_computer.cross_psd(isk, isl);

        TFXX_debug(opt.debug, "croposp",
                   "CPSD " << TFXX_value(cpsd.data.size()));

        //cout << TFXX_value(frequencies.size()) << endl;
        // compute array of frequency values, if not yet done
        if (frequencies.size()<2)
        {
          if (opt.logscale && !opt.avg_on_output)
          {
            if (opt.verbose)
            {
              cout << "Compute logarithmic frequency scale" << endl;
            }
            frequencies=psd::log_frequency(cpsd.interval, cpsd.data.size(),
                                           opt.n_per_decade);
          }
          else
          {
            if (opt.verbose)
            {
              cout << "Compute linear frequency scale" << endl;
            }
            frequencies=psd::lin_frequency(cpsd.interval, cpsd.data.size());
          } // if (opt.logscale && !opt.avg_on_output) ... else
        } // if (frequencies.size()<1)

        croposp::NamedCPSD named_cpsd;
        if (opt.logscale && !opt.avg_on_output)
        {
          named_cpsd.series=psd::log_sampling(cpsd, frequencies);
        } // if (opt.logscale && !opt.avg_on_output)
        else
        {
          named_cpsd.series=cpsd.data;
        } // if (opt.logscale && !opt.avg_on_output) ... else

        TFXX_debug(opt.debug, "croposp",
                   "CPSD " << TFXX_value(named_cpsd.series.size()));

        named_cpsd.label=vector_of_metadata[k].label+" "
          +vector_of_metadata[l].label;
        CPSD_vector[pairs(k,l)]=named_cpsd;

        TFXX_debug(opt.debug, "croposp",
                   "CPSD " << TFXX_value(CPSD_vector[pairs(k,l)].label));

        if (opt.verbose)
        {
          cout << "  computed: " << named_cpsd.label << endl;
        }

      } // for (unsigned int l=0; l<k; ++l)
    } // for (unsigned int l=0; l<k; ++l)

  } // if (opt.compute_transfer || opt.compute_phase || opt.compute_npsd ||
    //     opt.compute_coherence)

  /* ---------------------------------------------------------------------- */
  /* compute magnitude squared coherence
   * -----------------------------------
   */

  if (opt.compute_coherence)
  {
    if (opt.verbose)
    {
      cout << endl 
        << "Compute coherence for all pairs of signals." 
        << endl;
    }

    croposp::TNamedSeriesVector coherence_vector(pairs.pairs());

    TFXX_assert(PSD_vector.size()==pairs.size(),
                "inconsistency; report this as a bug");
    TFXX_assert(CPSD_vector.size()==pairs.pairs(),
                "inconsistency; report this as a bug");

    for (unsigned int k=1; k<pairs.size(); ++k)
    {
      for (unsigned int l=0; l<k; ++l)
      {

        psd::TDseries::Tcoc mscpsd=psd::abssqr(CPSD_vector[pairs(k,l)].series);
        coherence_vector[pairs(k,l)].series
          =mscpsd/(PSD_vector[k].series*PSD_vector[l].series);
        coherence_vector[pairs(k,l)].label=opt.pattern_coherence;
        coherence_vector[pairs(k,l)].label
          =croposp::patsubst(coherence_vector[pairs(k,l)].label, "%1", 
                             vector_of_metadata[k].label);
        coherence_vector[pairs(k,l)].label
          =croposp::patsubst(coherence_vector[pairs(k,l)].label, "%2", 
                             vector_of_metadata[l].label);

        if (opt.verbose)
        {
          cout << "  computed: " << coherence_vector[pairs(k,l)].label << endl;
        }

      } // for (unsigned int l=0; l<k; ++l)
    } // for (unsigned int l=0; l<k; ++l)

    write_named_series(opt.outfile_coherence,
                       "coherence of pairs of signals",
                       frequencies, coherence_vector, opt);

  } // if (opt.compute_coherence)

  /* ---------------------------------------------------------------------- */
  /* compute amplitude phase function
   * --------------------------------
   */

  if (opt.compute_phase)
  {
    if (opt.verbose)
    {
      cout << endl 
        << "Compute amplitude phase transfer function for all pairs of signals." 
        << endl;
    }

    TFXX_assert(pairs.size()>2, "requires at least three input time series");

    croposp::Triples triples(pairs, false);
    croposp::TNamedSeriesVector phase_vector(triples.triples());

    if (opt.verbose)
    {
      cout << endl
        << "Provide storage for " << triples.triples() << " triples "
        << "composed from " << pairs.size() << " signals."
        << endl
        << "phase_vector size is " << phase_vector.size()
        << endl;
    }

    TFXX_assert(CPSD_vector.size()==pairs.pairs(),
                "inconsistency; report this as a bug");

    for (unsigned int k=1; k<pairs.size(); ++k)
    {
      for (unsigned int l=0; l<k; ++l)
      {
        for (unsigned int i=0; i<pairs.size(); ++i)
        {
          if ((k!=i) && (l!=i))
          {
            psd::TDCseries::Tcoc cpsd1, cpsd2;
            cpsd1=CPSD_vector[pairs(k,i)].series;
            if (pairs.swap(k,i)) { cpsd1 = psd::conj(cpsd1); }
            cpsd2=CPSD_vector[pairs(l,i)].series;
            if (pairs.swap(l,i)) { cpsd2 = psd::conj(cpsd2); }
            phase_vector[triples(i,k,l)].series=psd::arg(cpsd1/cpsd2);
            phase_vector[triples(i,k,l)].label=opt.pattern_phase;
            phase_vector[triples(i,k,l)].label
              =croposp::patsubst(phase_vector[triples(i,k,l)].label, "%1", 
                                 vector_of_metadata[k].label);
            phase_vector[triples(i,k,l)].label
              =croposp::patsubst(phase_vector[triples(i,k,l)].label, "%2", 
                                 vector_of_metadata[l].label);
            phase_vector[triples(i,k,l)].label
              =croposp::patsubst(phase_vector[triples(i,k,l)].label, "%3", 
                                 vector_of_metadata[i].label);
          }
        }
      }
    }

    write_named_series(opt.outfile_phase,
                       "phase transfer function of pairs of signals",
                       frequencies, phase_vector, opt);

  } // if (opt.compute_phase)

  /* ---------------------------------------------------------------------- */
  /* compute amplitude transfer function
   * -----------------------------------
   */

  if (opt.compute_transfer)
  {
    if (opt.verbose)
    {
      cout << endl 
        << "Compute amplitude transfer function for all pairs of signals." 
        << endl;
    }

    TFXX_assert(pairs.size()>2, "requires at least three input time series");

    croposp::Triples triples(pairs, false);
    croposp::TNamedSeriesVector transfer_vector(triples.triples());

    if (opt.verbose)
    {
      cout << endl
        << "Provide storage for " << triples.triples() << " triples "
        << "composed from " << pairs.size() << " signals."
        << endl
        << "transfer_vector size is " << transfer_vector.size()
        << endl;
    }

    TFXX_assert(CPSD_vector.size()==pairs.pairs(),
                "inconsistency; report this as a bug");

    for (unsigned int k=1; k<pairs.size(); ++k)
    {
      for (unsigned int l=0; l<k; ++l)
      {
        for (unsigned int i=0; i<pairs.size(); ++i)
        {
          if ((k!=i) && (l!=i))
          {
            psd::TDseries::Tcoc cpsd1, cpsd2;
            cpsd1=psd::abs(CPSD_vector[pairs(k,i)].series);
            cpsd2=psd::abs(CPSD_vector[pairs(l,i)].series);
            transfer_vector[triples(i,k,l)].series=cpsd1/cpsd2;
            transfer_vector[triples(i,k,l)].label=opt.pattern_transfer;
            transfer_vector[triples(i,k,l)].label
              =croposp::patsubst(transfer_vector[triples(i,k,l)].label, "%1", 
                                 vector_of_metadata[k].label);
            transfer_vector[triples(i,k,l)].label
              =croposp::patsubst(transfer_vector[triples(i,k,l)].label, "%2", 
                                 vector_of_metadata[l].label);
            transfer_vector[triples(i,k,l)].label
              =croposp::patsubst(transfer_vector[triples(i,k,l)].label, "%3", 
                                 vector_of_metadata[i].label);
          }
        }
      }
    }

    write_named_series(opt.outfile_transfer,
                       "amplitude transfer ratio of pairs of signals",
                       frequencies, transfer_vector, opt);

  } // if (opt.compute_transfer)

  /* ---------------------------------------------------------------------- */
  /* compute incoherent component
   * ----------------------------
   */

  if (opt.compute_npsd)
  {
    if (opt.verbose)
    {
      cout << endl 
        << "Compute incoherent component." 
        << endl;
      cout << "selected mode: "
        << npsd_mode_label[opt.npsd_mode] << endl;
    }

    TFXX_assert(pairs.size()>2, "requires at least three input time series");

    croposp::Triples triples(pairs, true);
    croposp::TNamedSeriesVector npsd_vector(triples.triples());

    if (opt.verbose)
    {
      cout << endl
        << "Provide storage for " << triples.triples() << " triples "
        << "composed from " << pairs.size() << " signals."
        << endl
        << "npsd_vector size is " << npsd_vector.size()
        << endl;
    }

    TFXX_assert(PSD_vector.size()==pairs.size(),
                "inconsistency; report this as a bug");
    TFXX_assert(CPSD_vector.size()==pairs.pairs(),
                "inconsistency; report this as a bug");

    // compute logarithmic frequency sampling, if required
    psd::TDseries logfreq=frequencies;
    if (opt.avg_complex) 
    {
      logfreq=psd::log_frequency(frequencies, opt.n_per_decade);
    }

    for (unsigned int i=0; i<pairs.size(); ++i)
    {
      psd::TDseries::Tcoc psd=PSD_vector[i].series.copyout();
      if (opt.avg_complex)
      {
        psd=psd::log_sampling(psd, frequencies, logfreq);
      }

      for (unsigned int k=1; k<pairs.size(); ++k)
      {
        for (unsigned int l=0; l<k; ++l)
        {
          if ((k!=i) && (l!=i))
          {
            psd::TDCseries::Tcoc Cki,Cil,Ckl;

            if (pairs.swap(k,i))
            { Cki=psd::conj(CPSD_vector[pairs(k,i)].series); }
            else
            { Cki=CPSD_vector[pairs(k,i)].series; }

            if (pairs.swap(i,l))
            { Cil=psd::conj(CPSD_vector[pairs(i,l)].series); }
            else
            { Cil=CPSD_vector[pairs(i,l)].series; }

            if (pairs.swap(k,l))
            { Ckl=psd::conj(CPSD_vector[pairs(k,l)].series); }
            else
            { Ckl=CPSD_vector[pairs(k,l)].series; }

            psd::TDCseries coherent_psd=(Cki*Cil/Ckl);
            if (opt.avg_complex)
            {
              coherent_psd=::psd::log_sampling(coherent_psd,
                                             frequencies, logfreq);
            }

            psd::TDseries npsd(coherent_psd.shape());

            aff::Iterator<psd::TDseries> iNPSD(npsd);
            aff::Browser<psd::TDCseries::Tcoc> iCPSD(coherent_psd);
            aff::Browser<psd::TDseries::Tcoc> iPSD(psd);

            while(iNPSD.valid() && iCPSD.valid() && iPSD.valid())
            {
              switch (opt.npsd_mode) {
                case 0:
                  *iNPSD = std::abs(*iPSD - (*iCPSD));
                  break;
                case 1:
                  *iNPSD = std::abs(*iPSD-std::abs(*iCPSD));
                  break;
                case 2:
                  *iNPSD = std::abs(*iPSD-(iCPSD->real()));
                  break;
                case 3:
                  *iNPSD = std::abs(iCPSD->imag());
                  break;
                default:
                  TFXX_abort("illegal npsd mode");
              }
              ++iNPSD;
              ++iCPSD;
              ++iPSD;
            }

            npsd_vector[triples(i,k,l)].series=npsd;
            npsd_vector[triples(i,k,l)].label=opt.pattern_npsd;
            npsd_vector[triples(i,k,l)].label
              =croposp::patsubst(npsd_vector[triples(i,k,l)].label, "%1", 
                                 vector_of_metadata[i].label);
            npsd_vector[triples(i,k,l)].label
              =croposp::patsubst(npsd_vector[triples(i,k,l)].label, "%2", 
                                 vector_of_metadata[k].label);
            npsd_vector[triples(i,k,l)].label
              =croposp::patsubst(npsd_vector[triples(i,k,l)].label, "%3", 
                                 vector_of_metadata[l].label);
            npsd_vector[triples(i,k,l)].label
              =croposp::patsubst(npsd_vector[triples(i,k,l)].label, "%M", 
                                 npsd_mode_label[opt.npsd_mode]);
          }
        }
      }
    }

    if (opt.avg_complex)
    {
      croposp::Options outopt=opt;
      outopt.avg_on_output=false;
      write_named_series(opt.outfile_npsd,
                         "power spectral density of incoherent component",
                         logfreq, npsd_vector, outopt);
    }
    else
    {
      write_named_series(opt.outfile_npsd,
                         "power spectral density of incoherent component",
                         frequencies, npsd_vector, opt);
    }

  } // if (opt.compute_npsd)

} // main()

/* ----- END OF croposp.cc ----- */
