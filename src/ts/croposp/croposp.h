/*! \file croposp.h
 * \brief declaration of classes, types and functions used in croposp 
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 06/02/2019
 * 
 * declaration of classes, types and functions used in croposp (prototypes)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 06/02/2019   V1.0   Thomas Forbriger
 *  - 14/02/2019   V1.1   set program version 1.1
 *  - 08/04/2019   V1.2   introduce option avgcomplex
 * 
 * ============================================================================
 */
#define CROPOSP_VERSION \
  "CROPOSP   V1.5   Cross power spectral density"

// include guard
#ifndef CROPOSP_H_VERSION

#define CROPOSP_H_VERSION \
  "CROPOSP_H   V1.2"

#include <string>
#include <vector>
#include <tfxx/commandline.h>
#include <tfxx/xcmdline.h>
#include <tsxx/wid2timeseries.h>
#include <tsxx/tscollection.h>
#include <psdxx/psd.h>

namespace croposp {

  // a structure to hold all commanline parameters
  struct Options {
    bool verbose, trim, debug, overwrite;
    std::string inputformat, labelpattern;
    double datetolerance;
    bool logscale, avg_on_output, avg_complex;
    unsigned int n_per_decade;
    unsigned int nsegments, divisor, padfactor;
    double overlap;
    bool compute_psd, compute_npsd, compute_coherence, compute_transfer;
    bool compute_phase;
    std::string outfile_psd, outfile_npsd, outfile_coherence, outfile_transfer;
    std::string outfile_phase;
    std::string pattern_psd, pattern_npsd, pattern_transfer;
    std::string pattern_phase, pattern_coherence;
    unsigned int npsd_mode;
  }; // struct Options

  // a struct to hold meta data
  struct Meta {
    tfxx::cmdline::Filename filearguments;
    std::string label;
  }; // struct Meta

  // type of time series collection to hold input data
  typedef ts::TimeSeriesCollection<double> TCollection;
  // a vector to gold meta data for each time series
  typedef std::vector<Meta> TMetaVector;

  // a struct to hold computation results for exactly one series
  struct NamedSeries {
    std::string label;
    psd::TDseries::Tcoc series;
  }; // struct NamedSeries

  // a struct to hold computation results cross PSD
  struct NamedCPSD {
    std::string label;
    psd::TDCseries::Tcoc series;
  }; // struct NamedCPSD

  // a vector type to hold results
  typedef std::vector<NamedSeries> TNamedSeriesVector;

  // a vector type to hold results
  typedef std::vector<NamedCPSD> TNamedCPSDVector;

  /* ====================================================================== */
  /* classes
   * -------
   */

  /*! provide index values to pairs 
   *
   * cycle through a complete set
   */
  class Pairs {
    public:
      Pairs(const unsigned int& n);
      unsigned int operator()(const unsigned int& k,
                              const unsigned int& l) const;
      bool swap(const unsigned int& k,
                const unsigned int& l) const;
      unsigned int size() const;
      unsigned int pairs() const;
    private:
      unsigned int Mn;
  }; // class Pairs

  /* ---------------------------------------------------------------------- */

  /*! provide index values to triples 
   *
   * cycle through a complete set
   */
  class Triples {
    public:
      Triples(const Pairs& p, const bool& pairs_first=false);
      unsigned int operator()(const unsigned int& i,
                              const unsigned int& k,
                              const unsigned int& l) const;
      bool swap(const unsigned int& k,
                const unsigned int& l) const;
      unsigned int size() const;
      unsigned int pairs() const;
      unsigned int triples() const;
    private:
      Pairs Mpairs;
      bool Mpairs_first;
  }; // class Triples

  /* ====================================================================== */
  /* functions
   * ---------
   */

  std::string patsubst(const std::string& templatestring,
                       const std::string& pattern,
                       const std::string& content);

  void report_collection(const TCollection& collection,
                         const TMetaVector& metadata,
                         const bool& debug=false);

  void write_named_series(const std::string& filename,
                          const std::string& comment,
                          psd::TDseries f,
                          const TNamedSeriesVector& nsv,
                          const Options& opt);

} // namespace croposp

#endif // TF_CROPOSP_H_VERSION (includeguard)

/* ----- END OF croposp.h ----- */
