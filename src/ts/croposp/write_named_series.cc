/*! \file write_named_series.cc
 * \brief write named series to output file (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 06/02/2019
 * 
 * write named series to output file (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 06/02/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define TF_WRITE_NAME_SERIES_CC_VERSION \
  "TF_WRITE_NAME_SERIES_CC   V1.0"

#include "croposp.h"
#include<iostream>
#include<fstream>

using std::cout;
using std::endl;

namespace croposp {

  void write_named_series(const std::string& filename,
                          const std::string& comment,
                          psd::TDseries f,
                          const TNamedSeriesVector& nsv,
                          const Options& opt)
  {
    if (opt.verbose)
    {
      cout << endl
        << "output to file " << filename << ":" << endl
        << comment << endl;
    }
    if (!opt.overwrite)
    {
      std::ifstream file(filename.c_str(), std::ios_base::in);
      TFXX_assert((!file.good()),"ERROR: output file exists!");
    }
    TFXX_assert(nsv.size()>0, "data container is empty");
    std::ofstream os(filename.c_str());
    os << "# " << CROPOSP_VERSION << endl;
    os << "# " << comment << endl;
    for (unsigned int i=0; i<nsv.size(); ++i)
    {
      unsigned int fi=i+1;
      os << "# #" << fi << ": " << nsv[i].label << endl;
      TFXX_assert(nsv[i].series.size()==f.size(),
                  "series passed to output function are inconsistent")
    }
    // set first index to 0 - just in case
    f.shift(-f.first());

    // map to logarithmic frequency scale if requested
    psd::TDseries linfreq=f;
    psd::TDseries freq=f;
    if (opt.logscale && opt.avg_on_output)
    {
      freq=psd::log_frequency(linfreq, opt.n_per_decade);
    }
    std::vector<psd::TDseries::Tcoc> vector_of_series(nsv.size());
    for (unsigned int i=0; i<nsv.size(); ++i)
    {
      if (opt.logscale && opt.avg_on_output)
      {
        vector_of_series[i]=psd::log_sampling(nsv[i].series, linfreq, freq);
      }
      else
      {
        vector_of_series[i]=nsv[i].series;
      }
    }

    unsigned int nsamples=freq.size();
    for (unsigned int i=0; i<nsamples; ++i)
    {
      os << freq(i);
      for (unsigned int j=0; j<nsv.size(); ++j)
      {
        const psd::TDseries::Tcoc& s=vector_of_series[j];
        os << " " << s(i+s.first());
      }
      os << endl;
    }
  } // void write_named_series(const std::string& filename,
  //                         const std::string& comment,
  //                         psd::TDseries f,
  //                         const TNamedSeriesVector& nsv,
  //                         const Options& opt)

} // namespace croposp

/* ----- END OF write_named_series.cc ----- */
