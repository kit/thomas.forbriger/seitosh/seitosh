/*! \file pairs.cc
 * \brief pairs manipulator (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 06/02/2019
 * 
 * pairs manipulator (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 06/02/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define TF_PAIRS_CC_VERSION \
  "TF_PAIRS_CC   V1.0"

#include "croposp.h"
#include<tfxx/error.h>

namespace croposp {

  /*! \class croposp::Pairs
   *
   * Handle pairs of data.
   *
   * The constructor takes the size of the collection.
   * Index values must be positive (zero or larger than zero) and smaller than
   * the size of the collection.
   */

  Pairs::Pairs(const unsigned int& n): Mn(n) 
  { 
    TFXX_assert(this->Mn>1, 
                "collection must have at least two elements to make a pair");
  }

  /* ---------------------------------------------------------------------- */

  unsigned int Pairs::operator()(const unsigned int& k,
                                 const unsigned int& l) const
  {
    unsigned int retval=0;
    TFXX_assert(k!=l, "index values must not equal");
    if (this->swap(k, l))
    {
      TFXX_assert(l<this->Mn, "index value too large");
      TFXX_assert(k>=0, "index value too small");
      retval=l*(l-1)/2+k;
    }
    else
    {
      TFXX_assert(k<this->Mn, "index value too large");
      TFXX_assert(l>=0, "index value too small");
      retval=k*(k-1)/2+l;
    }
    return (retval);
  } // unsigned int Pairs::operator(const unsigned int& k,
    //                              const unsigned int& l) const

  /* ---------------------------------------------------------------------- */

  bool Pairs::swap(const unsigned int& k,
                   const unsigned int& l) const
  {
    return(k<=l);
  } // bool Pairs::swap(const unsigned int& k,
    //                  const unsigned int& l) const

  /* ---------------------------------------------------------------------- */

  unsigned int Pairs::size() const
  {
    return(this->Mn);
  } // unsigned int Pairs::size() const

  /* ---------------------------------------------------------------------- */

  unsigned int Pairs::pairs() const
  {
    TFXX_assert(this->size()>1, "size is too small to build pairs");
    unsigned int retval=(this->Mn*(this->Mn-1))/2;
    return(retval);
  } // unsigned int Pairs::size() const

} // namespace croposp

/* ----- END OF pairs.cc ----- */
