/*! \file triples.cc
 * \brief Handle index values for triples of time series (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 08/02/2019
 * 
 * Handle index values for triples of time series (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 08/02/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define TF_TRIPLES_CC_VERSION \
  "TF_TRIPLES_CC   V1.0"

#include "croposp.h"
#include<tfxx/error.h>

  /*! \class croposp::Triples
   *
   * Handle triples of signals.
   *
   * The constructor takes the Pairs to be handled in the triples
   * Index values must be positive (zero or larger than zero) and smaller than
   * the size of the collection.
   */

namespace croposp {

  Triples::Triples(const Pairs& p, const bool& pairs_first):
    Mpairs(p), Mpairs_first(pairs_first) 
  { 
    TFXX_assert(this->size()>2, 
          "collection must have at least three elements to make a triple");
  } // Triples::Triples(const Pairs& p, const bool& pairs_first=false):

  /* ---------------------------------------------------------------------- */

  unsigned int Triples::operator()(const unsigned int& i,
                                   const unsigned int& k,
                                   const unsigned int& l) const
  {
    unsigned int retval=0;
    unsigned int size=this->size();
    TFXX_assert((i!=k) && (k!=l) && (l!=i),
                "triple indices must be unique; report this as a bug");
    TFXX_assert((i<size) && (k<size) && (l<size),
                "index value(s) out of range; report this as a bug");
    if (this->Mpairs_first)
    {
      croposp::Pairs pairs(size-1);
      unsigned int kk=k;
      unsigned int ll=l;
      if (kk>i) { --kk; }
      if (ll>i) { --ll; }
      retval=pairs(kk,ll)+i*pairs.pairs();
    }
    else
    {
      unsigned int ii=i;
      if (ii>k) { --ii; }
      if (ii>l) { --ii; }
      retval=this->Mpairs.operator()(k,l)*(this->size()-2)+ii;
    }
    return(retval);
  } // unsigned int Triples::operator()(const unsigned int& i,
    //                                  const unsigned int& k,
    //                                  const unsigned int& l) const

  /* ---------------------------------------------------------------------- */

  bool Triples::swap(const unsigned int& k,
                     const unsigned int& l) const
  {
    return(this->Mpairs.swap(k, l));
  } // bool Triples::swap(const unsigned int& k,
    //                    const unsigned int& l) const

  /* ---------------------------------------------------------------------- */

  unsigned int Triples::size() const
  {
    return(this->Mpairs.size());
  } // unsigned int Triples::size() const

  /* ---------------------------------------------------------------------- */

  unsigned int Triples::pairs() const
  {
    return(this->Mpairs.pairs());
  } // unsigned int Triples::pairs() const

  /* ---------------------------------------------------------------------- */

  unsigned int Triples::triples() const
  {
    unsigned int n=this->size();
    unsigned int retval=n*(n-1)*(n-2)/2;
    return(retval);
  } // unsigned int Triples::triples() const

} // namespace croposp

/* ----- END OF triples.cc ----- */
