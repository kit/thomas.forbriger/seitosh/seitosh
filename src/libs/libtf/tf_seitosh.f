c this is <tf_seitosh.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
c
c provide references to Seitosh
c
c ----
c This program is free software; you can redistribute it and/or modify
c it under the terms of the GNU General Public License as published by
c the Free Software Foundation; either version 2 of the License, or
c (at your option) any later version. 
c 
c This program is distributed in the hope that it will be useful,
c but WITHOUT ANY WARRANTY; without even the implied warranty of
c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
c GNU General Public License for more details.
c 
c You should have received a copy of the GNU General Public License
c along with this program. If not, see <http://www.gnu.org/licenses/>.
c ----
c
c REVISIONS and CHANGES
c    27/02/2019   V1.0   Thomas Forbriger
c
c ============================================================================
c
cS
      subroutine tf_seitosh_reference
c
c print a reference to Seitosh
c typically used as part of the usage information
c
cE
      print *,'This program is a part of Seitosh: ',
     &        'https://gitlab.kit.edu/kit/thomas.forbriger/seitosh/seitosh'
      return
      end
c
c ----- END OF tf_seitosh.f ----- 
