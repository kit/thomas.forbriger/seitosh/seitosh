/*! \file rngtest.cc
 * \brief test random number generator module
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 31/10/2019
 * 
 * test random number generator module
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * REVISIONS and CHANGES 
 *  - 31/10/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define RNGTEST_VERSION \
  "RNGTEST   V1.0   test random number generator module"

#include <iostream>
#include <tfxx/commandline.h>
#include <tfxx/rng.h>

using std::cout;
using std::cerr;
using std::endl;

/* ---------------------------------------------------------------------- */

struct Options {
  double mean,std;
  unsigned long int seed;
  bool seedset,verbose,listtypes;
  std::string type;
  int nsamples;
}; // struct Options

/* ---------------------------------------------------------------------- */

void report(const tfxx::numeric::RNGgaussian& rng)
{
  cout << "RNG parameters:\n"
    << " type: " << rng.type() << "\n"
    << " seed: " << rng.seed() << "\n"
    << " mean: " << rng.mean() << "\n"
    << " std:  " << rng.std() << endl;
} // void report(const tfxx::numeric::RNGgaussian& rng)

/* ---------------------------------------------------------------------- */

int main(int iargc, char* argv[])
{

  // define usage information
  char usage_text[]=
  {
    RNGTEST_VERSION "\n"
    "usage: rngtest [-l] [-v] [-t type] [-seed v] [-mean v]\n"
    "               [-std v] [-n n] [run]\n"
    "   or: rngtest --help|-h" "\n"
    "   or: rngtest --xhelp" "\n"
  };

  // define full help text
  char help_text[]=
  {
    "--xhelp      print comments regarding GSL RNG implementation\n"
    "\n"
    "run          run the program\n"
    "-l           print list of random number generators and exit\n"
    "-v           be verbose\n"
    "-t type      select random number generator of type \"type\"\n"
    "             use option -l for a list of available types\n"
    "             type \"default\" will make selection based on\n"
    "             environment variables\n"
    "-seed v      initialize RNG with seed value v\n"
    "-mean v      set mean value\n"
    "-std v       set standard deviation\n"
    "-n n         print n random numbers\n"
    "\n"
    "In case of no type selected or type set to \"default\" type\n"
    "and seed value can be specified through environment variables.\n"
    "Simply try\n"
    "\n"
    "GSL_RNG_TYPE=ranlux GSL_RNG_SEED=125 rngtest -v run\n"
  };

  // define commandline options
  using namespace tfxx::cmdline;
  static Declare options[]= 
  {
    // 0: print help
    {"help",arg_no,"-"},
    // 1: verbose mode
    {"v",arg_no,"-"},
    // 2: print list of random number generators
    {"l",arg_no,"-"},
    // 3: type of RNG
    {"t",arg_yes,"default"},
    // 4: seed value
    {"seed",arg_yes,"0."},
    // 5: mean value
    {"mean",arg_yes,"0."},
    // 6: standard deviation
    {"std",arg_yes,"1."},
    // 7: number of samples to be printed
    {"n",arg_yes,"10"},
    // 8: print comment text
    {"xhelp",arg_no,"-"},
    {NULL}
  };

  // no arguments? print usage...
  if (iargc<2) 
  {
    cerr << usage_text << endl;
    exit(0);
  }

  // collect options from commandline
  Commandline cmdline(iargc, argv, options);

  // help requested? print full help text...
  if (cmdline.optset(0) || cmdline.optset(8))
  {
    cerr << usage_text << endl;
    cerr << help_text << endl;
    if (cmdline.optset(8))
    {
      cerr << endl;
      cerr << tfxx::numeric::RNGgaussian::comment_gsl_rng_ranlux << endl;
    }
    exit(0);
  }

  Options opt;
  opt.verbose=cmdline.optset(1);
  opt.listtypes=cmdline.optset(2);
  opt.type=cmdline.string_arg(3);
  opt.seedset=cmdline.optset(4);
  opt.seed=cmdline.long_arg(4);
  opt.mean=cmdline.double_arg(5);
  opt.std=cmdline.double_arg(6);
  opt.nsamples=cmdline.int_arg(7);

  if (opt.listtypes)
  {
    tfxx::numeric::RNGgaussian::rng_list_types(cout);
  }
  else
  {
    cout << RNGTEST_VERSION << endl;
    if (opt.verbose)
    {
      cout << "Parameters specified on command line (maybe default):\n"
        << "  type: " << opt.type << "\n"
        << "  mean: " << opt.mean << "\n"
        << "  std:  " << opt.std << "\n"
        << "  n:    " << opt.nsamples << endl;
      if (opt.seedset)
      {
        cout << "  seed: " << opt.seed << endl;
      }
      else
      {
        cout << "  no seed value specified" << endl;
      }
    }
    // create RNG
    tfxx::numeric::RNGgaussian rng(opt.std, opt.mean, opt.type.c_str());
    if (opt.seedset) { rng.set(opt.seed); }
    // report
    if (opt.verbose) { report(rng); }
    
    cout << "\nRandom numbers:" << endl;
    for (int i=0; i<opt.nsamples; ++i)
    {
      cout << i << " " << rng.value() << endl;
    }

    cout << endl << "set new seed" << endl;
    rng.set();
    if (opt.verbose) { report(rng); }
    
    cout << "\nRandom numbers:" << endl;
    for (int i=0; i<opt.nsamples; ++i)
    {
      cout << i << " " << rng.value() << endl;
    }
  }
}

/* ----- END OF rngtest.cc ----- */
