/*! \file rng.h
 * \brief interface to GSL random number generators (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 19/12/2007
 * 
 * interface to GSL random number generators (prototypes)
 * 
 * Copyright (c) 2007 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * ----
 * 
 * REVISIONS and CHANGES 
 *  - 19/12/2007   V1.0   Thomas Forbriger
 *  - 01/11/2019   V1.1   support selection of random number generator type
 * 
 * ============================================================================
 */

// include guard
#ifndef TF_RNG_H_VERSION

#define TF_RNG_H_VERSION \
  "TF_RNG_H   V1.1"

#include <tfxx/error.h>
#include <ostream>
#include <string>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

/*! \defgroup group_numeric Numerical functions.
 * \brief Provide numerical functions, random numbers, etc.
 *
 * The module is presented in namespace tfxx::numeric.
 */

namespace tfxx {

  /*! \namespace tfxx::numeric
   * \brief Namespace containing all components of module numeric.
   * \ingroup group_numeric
   * \sa group_numeric
   */
  namespace numeric {

  /* ---------------------------------------------------------------------- */

    /*! \brief Provide random numbers with Gaussian distribution.
     * \ingroup group_numeric
     *
     * This class is an interface to the GSL random number generator.
     * \sa https://www.gnu.org/software/gsl/doc/html/rng.html
     *
     * \note
     * This class specifically returns values based on a normal distribution.
     * If for some reason a second class shall be provided to return values
     * based on a uniform distribution, only the value() member function will
     * be different. It then would be appropriate to define an abstract base
     * class from which RNGgaussion and RNGuniform are derived.
     */
    class RNGgaussian {
      public:
        //! initialize random number generator
        explicit RNGgaussian(const double& std=1.,
                             const double& mean=0.,
                             const char* type="default");
        ~RNGgaussian();
        //! returns a random number
        double operator()() const;
        //! returns a random number
        double value() const;
        //! feed with seed value
        void set(const unsigned long int& seed);
        //! use time as seed value
        void set();
        //! return type of random number generator
        std::string type() const {return std::string(MR->type->name); }
        //! return last seed value
        unsigned long int seed() const { return Mseed; }
        //! return standard deviation
        double std() const { return Mstd; }
        //! return mean
        double mean() const { return Mmean; }
        //! print list of random number generators to stream.
        static void rng_list_types(std::ostream& os);
        //! comment on GSL RNGs
        static const char* comment_gsl_rng_ranlux;
      private:
        //! store standard deviation and mean value
        double Mstd, Mmean;
        //! pointer to RNG
        gsl_rng* MR;
        //! memorize last seed value
        unsigned long int Mseed;
      public:
        /*! \brief exception class for RNG indicating request for unkown type
         *
         * \sa TFXX_Xassert
         * \sa tfxx::error::Exception
         */
        class UTException: public tfxx::error::Exception
      {
        public:
          //! base class
          typedef tfxx::error::Exception TBase;
          //! Create with message, failed assertion, and code position
          UTException(const char* type, 
                      const char* file,
                      const int& line,
                      const char* condition);
          //! provide explicit virtual destructor
          virtual ~UTException() { }
        private:
          //! pointer name string of requested RNG type
          const char* Mtype;
      }; // class UTException
    }; // class RNGgaussian
  } // namespace numeric

} // namespace tfxx

#endif // TF_RNG_H_VERSION (includeguard)

/* ----- END OF rng.h ----- */
