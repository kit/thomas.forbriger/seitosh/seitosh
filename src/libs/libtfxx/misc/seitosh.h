/*! \file seitosh.h
 * \brief provide references to Seitosh (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 27/02/2019
 * 
 * provide references to Seitosh (prototypes)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 27/02/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */

// include guard
#ifndef TF_SEITOSH_H_VERSION

#define TF_SEITOSH_H_VERSION \
  "TF_SEITOSH_H   V1.0"

namespace tfxx {

  /*! references to Seitosh
   *
   * \sa https://gitlab.kit.edu/kit/thomas.forbriger/seitosh/seitosh
   */
  namespace seitosh {

    /*! reference to gitlab repository
     *
     * A short reference to the gitlab repository
     */
    extern const char* const repository_reference;

  } // namespace seitosh

} // namespace tfxx

#endif // TF_SEITOSH_H_VERSION (includeguard)

/* ----- END OF seitosh.h ----- */
