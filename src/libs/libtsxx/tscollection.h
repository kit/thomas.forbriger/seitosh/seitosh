/*! \file tscollection.h
 * \brief provide a container for a collection of time series with header data (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 29/12/2018
 * 
 * provide a container for a collection of time series with header data
 * (prototypes)
 * 
 * Copyright (c) 2018 by Thomas Forbriger (BFO Schiltach) 
 * 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 29/12/2018   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */

// include guard
#ifndef TS_TSCOLLECTION_H_VERSION

#define TS_TSCOLLECTION_H_VERSION \
  "TS_TSCOLLECTION_H   V1.0"

#include<vector>
#include<tsxx/error.h>
#include<tsxx/wid2timeseries.h>

namespace ts {

  /*! \brief A class template for a list of time series with SFF header.
   *
   * A TimeSeriesCollection is a vector of ts::TimeSeries with some additional
   * member functions.
   *
   * \param T value type of series container
   */
  template<typename T>
    class TimeSeriesCollection: 
      public std::vector<TimeSeries<aff::Series<T>, ::sff::WID2> >
  {
    public:
      /*! \name Type definitions.
       *
       * Provided to define proper base types of class template
       */
      //!@{
      typedef T Tvalue;
      typedef aff::Series<Tvalue> Tseries;
      typedef ::sff::WID2 Theader;
      typedef ts::TimeSeries<Tseries, Theader> Ttimeseries;
      typedef ts::TimeSeries<typename Tseries::Tcoc, Theader> Tconsttimeseries;
      typedef std::vector<Ttimeseries> Tbase;
      //!@}
      
      //! Check consistency
      bool are_consistent(const ::sff::WID2compare comparer) const;
      //! Check whether all time series have a common time window
      bool overlap() const;
      //! Synchronize header with series
      void synchronize_nsamples();
      //!  Trim to same start time
      void trim_to_date();
      //! Trim to same number of samples
      void trim_to_nsamples();

  }; // class TimeSeriesCollection

  /* ---------------------------------------------------------------------- */

  /*! \brief Check consistency
   *
   * Check the consistency of the time series in the collections based on
   * header data. For example this can be used to check whether all time
   * series have the same number of samples.
   *
   * \param comparer defines the header fields to be compared
   * \return true, if comparer returns true for comparison of all pairs
   *         of time series headers in the collection
   */
  template<typename T>
    bool TimeSeriesCollection<T>::are_consistent(const ::sff::WID2compare comparer) const
    {
      bool retval=true;
      if (this->size() > 1)
      {
        typename Tbase::const_iterator i_series=this->begin();
        Theader refheader=i_series->header;
        ++i_series;
        while ((i_series != this->end()) && retval)
        {
          retval=comparer(refheader, i_series->header);
          ++i_series;
        }
      }
      return(retval);
    } // bool TimeSeriesCollection<T>::are_consistent(const ::sff::WID2compare comparer) const

  /* ---------------------------------------------------------------------- */

  /*! \brief Check whether all time series have a common time window
   *
   * Check whether all time series overlap in time. This is a necessary
   * prerequisite for trimming all time series to a common time window.
   *
   * \return true, if time series overlap
   */
  template<typename T>
    bool TimeSeriesCollection<T>::overlap() const
    {
      bool retval=true;
      if (this->size() > 1)
      {
        typename Tbase::const_iterator i_series=this->begin();
        Theader header=i_series->header;
        libtime::TAbsoluteTime begin=header.date;
        libtime::TAbsoluteTime end=::sff::wid2lastsample(header);
        ++i_series;
        while ((i_series != this->end()) && retval)
        {
          header=i_series->header;
          libtime::TAbsoluteTime thisbegin=header.date;
          libtime::TAbsoluteTime thisend=::sff::wid2lastsample(header);
          begin = thisbegin > begin ? thisbegin : begin;
          end = thisend < end ? thisend : end;
          ++i_series;
        }
        retval=(end >= begin);
      }
      return(retval);
    } // bool TimeSeriesCollection<T>::overlap() const

  /* ---------------------------------------------------------------------- */

  /*! \brief Synchronize header with series
   *
   * Set actual number of samples and nsamples in header to the smaller of
   * both.
   */
  template<typename T>
    void TimeSeriesCollection<T>::synchronize_nsamples() 
    {
      typename Tbase::iterator i_series=this->begin();
      while (i_series != this->end())
      {
        const unsigned int& header_nsamples=i_series->header.nsamples;
        unsigned int series_nsamples=i_series->size();
        unsigned int nsamples=
          series_nsamples < header_nsamples 
          ? series_nsamples : header_nsamples;
        i_series->header.nsamples=nsamples;
        i_series->setlastindex(i_series->f()+nsamples-1);
        ++i_series;
      }
    } // void TimeSeriesCollection<T>::synchronize_nsamples() 

  /* ---------------------------------------------------------------------- */

  /*! \brief Trim to same start time
   *
   * Set all start dates to the latest found in the collection.
   */
  template<typename T>
    void TimeSeriesCollection<T>::trim_to_date() 
    {
      if (this->size() > 1)
      {
        // find lastest start date
        typename Tbase::iterator i_series=this->begin();
        Theader header=i_series->header;
        libtime::TAbsoluteTime begin=header.date;
        ++i_series;
        while (i_series != this->end())
        {
          header=i_series->header;
          libtime::TAbsoluteTime thisbegin=header.date;
          begin = thisbegin > begin ? thisbegin : begin;
          ++i_series;
        }

        // adjust series and header
        i_series=this->begin();
        while (i_series != this->end())
        {
          header=i_series->header;
          long int index_offset=wid2isample(header, begin);
          TSXX_assert(index_offset >= 0,
                      "inconsistent header data");
          TSXX_assert(index_offset < header.nsamples,
                      "time series does not overlap with others");
          i_series->header.date=wid2isample(header, index_offset);
          i_series->header.nsamples -= index_offset;
          i_series->setfirstindex(i_series->f()+index_offset);
          ++i_series;
        }
      }
    } // void TimeSeriesCollection<T>::trim_to_date() 

  /* ---------------------------------------------------------------------- */

  /*! \brief Trim to same number of samples
   *
   * Set length of all time series to smallest found in collection.
   */
  template<typename T>
    void TimeSeriesCollection<T>::trim_to_nsamples() 
    {
      this->synchronize_nsamples();
      if (this->size() > 1)
      {
        // find smallest number of samples in collection
        typename Tbase::iterator i_series=this->begin();
        Theader header=i_series->header;
        unsigned int nsamples=header.nsamples;
        ++i_series;
        while (i_series != this->end())
        {
          header=i_series->header;
          unsigned int thisnsamples=header.nsamples;
          nsamples = thisnsamples < nsamples ? thisnsamples : nsamples;
          ++i_series;
        }

        // adjust number of samples
        i_series=this->begin();
        while (i_series != this->end())
        {
          i_series->header.nsamples = nsamples;
          i_series->setlastindex(i_series->f()+nsamples-1);
          ++i_series;
        }
      }
    } // void TimeSeriesCollection<T>::trim_to_nsamples() 

  /* ---------------------------------------------------------------------- */

} // namespace ts

#endif // TS_TSCOLLECTION_H_VERSION (includeguard)

/* ----- END OF tscollection.h ----- */
