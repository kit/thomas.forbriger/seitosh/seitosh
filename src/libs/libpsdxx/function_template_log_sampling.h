/*! \file function_template_log_sampling.h
 * \brief implementation of function template for loagrithmic sampling (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 07/01/2019
 * 
 * implementation of function template for loagrithmic sampling (prototypes)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 07/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */

// include guard
#ifndef PSDXX_FUNCTION_TEMPLATE_LOG_SAMPLING_H_VERSION

#define PSDXX_FUNCTION_TEMPLATE_LOG_SAMPLING_H_VERSION \
  "PSDXX_FUNCTION_TEMPLATE_LOG_SAMPLING_H   V1.0"

#include <psdxx/psd.h>
#include <psdxx/helper.h>

namespace psd {

  namespace helper {

    /*! generic template function
     *
     * \param s spectral values with uniform sampling along frequency axis
     * \param f frequency values on a logarithmic scale like returned by
     *          function log_frequency()
     * \return series of sampled spectral values, where each sample of the
     *         returned series corresponds to the frequency with the
     *         corresponding index in \p f
     */
    template<typename T>
      aff::Series<T> 
      gen_log_sampling(const typename psd::IntervalSeries<T>::Tcoc& s,
                       const TDseries::Tcoc& f)
      {
        aff::Series<T> retval(f.f(), f.l());
        retval=T(0.);
        for (int i=f.f(); i<=f.l(); ++i)
        {
          psd::helper::LogTaper logtaper(f, i);
          /*! \todo
           * Sample spectral values and computed weighted average. logtaper
           * will return zero for most values and the computation of these
           * zeroes is quite expensive. There would be a more efficient way to
           * do the computation by limiting the sampling to the a-priori known
           * frequency interval of non-zero taper values. 
           * This is left to future improvements of the code.
           * See also comment in
           * double LogTaper::operator()(const double& f) 
           * \sa psd::helper::LogTaper::operator())()
           */
          for (int j=s.data.f(); j<=s.data.l(); ++j)
          {
            double freq=s.interval*(j-s.data.f());
            double taper=logtaper(freq);
            retval(i) += taper*s.data(j);
          }
          retval(i) /= logtaper.sum();
        } // for (int i=f.f(); i<=f.l(); ++i)
        return(retval);
      } // template<typename T>
        //   aff::Series<T> 
        //   gen_log_sampling(const typename psd::IntervalSeries<T>::Tcoc& s,
        //                    const TDseries::Tcoc& f)

  } // namespace helper

} // namespace psd

#endif // PSDXX_FUNCTION_TEMPLATE_LOG_SAMPLING_H_VERSION (includeguard)

/* ----- END OF function_template_log_sampling.h ----- */
