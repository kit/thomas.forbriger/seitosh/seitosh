/*! \file function_sqrt.cc
 * \brief return square root of real values in samples container (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 07/02/2019
 * 
 * return square root of real values in samples container (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 07/02/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_FUNCTION_SQRT_CC_VERSION \
  "PSDXX_FUNCTION_SQRT_CC   V1.0"

#include <psdxx/psd.h>
#include <aff/iterator.h>

namespace psd {

  TDseries sqrt(const TDseries::Tcoc& s)
  {
    TDseries retval(s.shape());

    aff::Iterator<TDseries> S(retval);
    aff::Browser<TDseries::Tcoc> C(s);

    while(S.valid() && C.valid())
    {
      *S = std::sqrt(*C);
      ++S; ++C;
    }
    return(retval);
  } // TDseries sqrt(const TDseries::Tcoc& s)

  /* ---------------------------------------------------------------------- */

  TDISeries sqrt(const TDISeries::Tcoc& s)
  {
    TDISeries retval;
    retval.interval=s.interval;
    retval.data=sqrt(s.data);
    return(retval);
  } // TDISeries sqrt(const TDISeries::Tcoc& s)

} // namespace psd

/* ----- END OF function_sqrt.cc ----- */
