/*! \file function_abssqr.cc
 * \brief square of magnitude (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 03/03/2019
 * 
 * square of magnitude (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 03/03/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_FUNCTION_ABSSQR_CC_VERSION \
  "PSDXX_FUNCTION_ABSSQR_CC   V1.0"

#include <psdxx/psd.h>
#include <aff/iterator.h>

namespace psd {

  TDseries abssqr(const TDCseries::Tcoc& s)
  {
    TDseries retval(s.shape());

    aff::Iterator<TDseries> S(retval);
    aff::Browser<TDCseries::Tcoc> C(s);

    while(S.valid() && C.valid())
    {
      *S = C->real()*C->real()+C->imag()*C->imag();
      ++S; ++C;
    }
    return(retval);
  } // TDseries abssqr(const TDCseries::Tcoc& s)

  /* ---------------------------------------------------------------------- */

  TDISeries abssqr(const TDCISeries::Tcoc& s)
  {
    TDISeries retval;
    retval.interval=s.interval;
    retval.data=abs(s.data);
    return(retval);
  } // TDISeries abssqr(const TDCISeries::Tcoc& s)

} // namespace psd

/* ----- END OF function_abssqr.cc ----- */
