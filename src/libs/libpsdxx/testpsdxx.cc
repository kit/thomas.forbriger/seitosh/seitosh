/*! \file testpsdxx.cc
 * \brief test library functions
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 10/01/2019
 * 
 * test library functions
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * REVISIONS and CHANGES 
 *  - 10/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define TESTPSDXX_VERSION \
  "TESTPSDXX   V1.1   test library functions"

#include <iostream>
#include <tfxx/commandline.h>
#include <psdxx/psd.h>
#include <psdxx/debug.h>
#include <psdxx/helper.h>
#include <aff/dump.h>

using std::cout;
using std::cerr;
using std::endl;

struct Options
{
  bool verbose, logfrequency, logindex, logtaper, logfreqtable;
  int taperindex, nsamples, nperdec;
  double fmin;
}; // Options

/* ====================================================================== */

void testlogindex(const psd::TDseries::Tcoc& f, const int& i)
{
  CODE( psd::helper::LogIndex index(f(i), f(i+1), f(i+2)) );
  cout << "with " << PSDXX_value(i) << endl;
  for (int i=f.f(); i<=f.l(); ++i)
  {
    cout << PSDXX_value(i) << " " << PSDXX_value(index(f(i))) << endl;
  }
}

/* ====================================================================== */

int main(int iargc, char* argv[])
{

  // define usage information
  char usage_text[]=
  {
    TESTPSDXX_VERSION "\n"
    "usage: testpsdxx [-lf] [-li] [-lt [i]]" "\n"
    "                 [-tab] [-fmin f] [-nsamples n] [-nperdec n]\n"
    "   or: testpsdxx --help|-h" "\n"
  };

  // define full help text
  char help_text[]=
  {
    "-lf     test function log_frequency\n"
    "-li     test function class LogIndex\n"
    "-lt [i] test function class LogTaper, center taper on\n"
    "        frequency with index i on logarithmic scale\n"
    "-tab    test function log_frequency with adjustable parameters\n"
    "\n"
    "-fmin f      set frequency interval on linear scale to f\n"
    "-nsamples n  set number of samples to n\n"
    "-nperdec n   set number of values per decade to n\n"
  };

  // define commandline options
  using namespace tfxx::cmdline;
  static Declare options[]= 
  {
    // 0: print help
    {"help",arg_no,"-"},
    // 1: verbose mode
    {"v",arg_no,"-"},
    // 2: log frequency
    {"lf",arg_no,"-"},
    // 3: log index
    {"li",arg_no,"-"},
    // 4: log taper
    {"lt",arg_opt,"5"},
    // 5: log scale with adjusted parameters
    {"tab",arg_no,"-"},
    // 6: smallest frequency
    {"fmin",arg_yes,"0.1"},
    // 7: number of samples
    {"nsamples",arg_yes,"10000"},
    // 8: number of values per decade
    {"nperdec",arg_yes,"5"},
    {NULL}
  };

  // no arguments? print usage...
  if (iargc<2) 
  {
    cerr << usage_text << endl;
    exit(0);
  }

  // collect options from commandline
  Commandline cmdline(iargc, argv, options);

  // help requested? print full help text...
  if (cmdline.optset(0))
  {
    cerr << usage_text << endl;
    cerr << help_text << endl;
    exit(0);
  }

  Options opt;
  opt.verbose=cmdline.optset(1);
  opt.logfrequency=cmdline.optset(2);
  opt.logindex=cmdline.optset(3);
  opt.logtaper=cmdline.optset(4);
  opt.taperindex=cmdline.int_arg(4);
  opt.logfreqtable=cmdline.optset(5);
  opt.fmin=cmdline.double_arg(6);
  opt.nsamples=cmdline.int_arg(7);
  opt.nperdec=cmdline.int_arg(8);

  if (opt.logfrequency)
  {
    cout << "test function log_frequency" << endl;
    CODE( psd::TDseries ls1=psd::log_frequency(0.1,100,6) );
    DUMP( ls1 );
    CODE( ls1=psd::log_frequency(0.1,10000,6) );
    DUMP( ls1 );
    CODE( ls1=psd::log_frequency(0.1,1000,20) );
    DUMP( ls1 );
    CODE( ls1=psd::log_frequency(0.1,10000,2) );
    DUMP( ls1 );
    CODE( ls1=psd::log_frequency(0.1,10000,6) );
    for (int i=ls1.f(); i<=ls1.l(); ++i)
    {
      cout << "i=" << i << " f=" << ls1(i) << " nu= " <<
        std::log(ls1(i)/ls1(ls1.f()))/std::log(10.) << endl;
    }
    int iref=2;
    double fref=ls1(iref-1);
    double fref1=ls1(iref);
    for (int i=iref+1; i<=ls1.l(); ++i)
    {
      cout << "i=" << i << " f=" << ls1(i) << " nu= " <<
        std::log((ls1(i)-fref)/(fref1-fref))/std::log(10.) << endl;
    }
  } // if (opt.logfrequency)

  /* ---------------------------------------------------------------------- */

  if (opt.logindex)
  {
    cout << "test class LogIndex" << endl;
    CODE( psd::TDseries ls1=psd::log_frequency(0.1,1000,6) );
    DUMP( ls1 );
    testlogindex(ls1, 0);
    testlogindex(ls1, 3);
  } // if (opt.logindex)

  /* ---------------------------------------------------------------------- */

  if (opt.logtaper)
  {
    const int nsamples=100;
    cout << "test class LogTaper" << endl;
    CODE( psd::TDseries linf=psd::lin_frequency(0.01,nsamples*10) );
    CODE( psd::TDseries logf=psd::log_frequency(0.1,nsamples,6) );
    CODE( psd::helper::LogTaper logt(logf, opt.taperindex) ); 
    DUMP( logf );
    cerr << PSDXX_value( logf(opt.taperindex) ) << endl;
    for (int i=linf.f(); i<= linf.l(); ++i)
    {
      cout << i << " " << linf(i) << " " << logt(linf(i)) << endl;
    }
    cerr << PSDXX_value( logt.sum() ) << endl;
  } // if (opt.logtaper)

  /* ---------------------------------------------------------------------- */

  if (opt.logfreqtable)
  {
    cout << "test log_frequency with adjustable parameters" << endl;
    cout << "  " << PSDXX_value( opt.fmin ) << endl;
    cout << "  " << PSDXX_value( opt.nsamples ) << endl;
    cout << "  " << PSDXX_value( opt.nperdec ) << endl;
    CODE( psd::TDseries ls1=psd::log_frequency(opt.fmin,opt.nsamples,opt.nperdec) );
    DUMP( ls1 );
    for (int i=ls1.f(); i<=ls1.l(); ++i)
    {
      cout << "i=" << i << " f=" << ls1(i) << " nu= " <<
        std::log(ls1(i)/ls1(ls1.f()))/std::log(10.) << endl;
    }
  } // if (opt.logfreqtable)
}

/* ----- END OF testpsdxx.cc ----- */
