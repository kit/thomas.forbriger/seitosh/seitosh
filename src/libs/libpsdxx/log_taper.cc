/*! \file log_taper.cc
 * \brief provide taper for averaging on a logarithmic scale (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 12/01/2019
 * 
 * provide taper for averaging on a logarithmic scale (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 12/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_LOG_TAPER_CC_VERSION \
  "PSDXX_LOG_TAPER_CC   V1.0"

#include <psdxx/helper.h>
#include <psdxx/error.h>
#include <psdxx/debug.h>
#include <cmath>

namespace psd {

  namespace helper {

    LogTaper::LogTaper(const psd::TDseries::Tcoc& f,
                       const unsigned int& i): Msum(0)
    {
      PSDXX_assert(f.size()>2,
                   "at least three values of frequency are required");
      PSDXX_assert((i>=f.f()) && (i<=f.l()),
                   "index is out of range");
      if (i==f.f())
      {
        this->Mloc=Flower_end;
        this->Mli=new psd::helper::LogIndex(f(i), f(i+1), f(i+2));
      }
      else if (i==f.l())
      {
        this->Mloc=Fupper_end;
        this->Mli=new psd::helper::LogIndex(f(i-2), f(i-1), f(i));
      }
      else
      {
        this->Mloc=Fmiddle;
        this->Mli=new psd::helper::LogIndex(f(i-1), f(i), f(i+1));
      }
    } // LogTaper::LogTaper(const psd::TDseries::Tcoc& f,
      //                    const unsigned int& i)

  /* ---------------------------------------------------------------------- */

    LogTaper::~LogTaper()
    {
      delete Mli;
    } // LogTaper::~LogTaper()

  /* ---------------------------------------------------------------------- */

    double LogTaper::operator()(const double& f) 
    {
      double retval=0;
      /*! \todo
       * there would be a more efficient way to find the interval of non-zero
       * taper values. This would require to store the boundary values of this
       * interval in the constructor. Calling the index function for every
       * frequency to find the position in the sequence, is computationally
       * very expensive and only necessary for values where taper values or to
       * be finite.
       * This is left to future improvements of the code.
       */
      double rindex=this->Mli->operator()(f);

      // taper function
      // 0.5*(1.-cos(ri*pi))

      if (this->Mloc==Flower_end)
      {
        // index of center frequency is 0
        // index range is from -1 to +1
        // taper is 1 for frequency index <= 0
        if ((rindex > 0.) && (rindex < 1.))
        {
          retval=0.5*(1.+std::cos(rindex*M_PI));
        }
        else if (rindex <= 0.)
        {
          retval=1.;
        }
      }
      else if (this->Mloc==Fupper_end)
      {
        // index of center frequency is 2
        // index range is from +1 to +3
        // taper is 1 for frequency index >= 2
        if ((rindex >= 1.) && (rindex < 2.))
        {
          retval=0.5*(1.+std::cos(rindex*M_PI));
          /*
          std::cerr << PSDXX_value(f) << " "
            << PSDXX_value(rindex) << " "
            << PSDXX_value(retval) << std::endl;
            */
        }
        else if (rindex >= 2.)
        {
          retval=1.;
        }
      }
      else if (this->Mloc==Fmiddle)
      {
        // index of center frequency is 1
        // index range is from 0 to +2
        if ((rindex > -0.) && (rindex < 2.))
        {
          retval=0.5*(1.-std::cos(rindex*M_PI));
        }
      }
      else
      {
        PSDXX_abort("illegal mode; this should never happen; "
                    "report this as a bug");
      }

      this->Msum += retval;
      return(retval);
    } // double LogTaper::operator()(const double& f) const

  } // namespace helper

} // namespace psd

/* ----- END OF log_taper.cc ----- */
