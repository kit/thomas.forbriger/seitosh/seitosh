/*! \file function_log_sampling_r.cc
 * \brief implementation for function log_sampling() for real values (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 07/01/2019
 * 
 * implementation for function log_sampling() for real values (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 07/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_FUNCTION_LOG_SAMPLING_R_CC_VERSION \
  "PSDXX_FUNCTION_LOG_SAMPLING_R_CC   V1.0"

#include <psdxx/psd.h>
#include <psdxx/function_template_log_sampling.h>

namespace psd {

   /*! sample real values on a logarithmic frequency axis
    */
   TDISeries::Tseries log_sampling(const TDISeries::Tcoc& s,
                                   const TDseries::Tcoc& f)
   {
     return(psd::helper::gen_log_sampling<TDISeries::Tvalue>(s, f));
   } // TDISeries::Tseries log_sampling(const TDISeries::Tcoc& s,
     //                                 const TDseries::Tcoc& f)

} // namespace psd

/* ----- END OF function_log_sampling_r.cc ----- */
