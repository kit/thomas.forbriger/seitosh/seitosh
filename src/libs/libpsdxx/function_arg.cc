/*! \file function_arg.cc
 * \brief return argument of complex values in samples container (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 05/01/2019
 * 
 * return argument of complex values in samples container (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 05/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_FUNCTION_ARG_CC_VERSION \
  "PSDXX_FUNCTION_ARG_CC   V1.0"

#include <psdxx/psd.h>
#include <aff/iterator.h>

namespace psd {

  TDseries arg(const TDCseries::Tcoc& s)
  {
    TDseries retval(s.shape());

    aff::Iterator<TDseries> S(retval);
    aff::Browser<TDCseries::Tcoc> C(s);

    while(S.valid() && C.valid())
    {
      *S = std::arg(*C);
      ++S; ++C;
    }
    return(retval);
  } // TDseries arg(const TDCseries::Tcoc& s)

  /* ---------------------------------------------------------------------- */

  TDISeries arg(const TDCISeries::Tcoc& s)
  {
    TDISeries retval;
    retval.interval=s.interval;
    retval.data=arg(s.data);
    return(retval);
  } // TDISeries arg(const TDCISeries::Tcoc& s)

} // namespace psd

/* ----- END OF function_arg.cc ----- */
