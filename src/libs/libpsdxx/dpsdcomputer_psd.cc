/*! \file dpsdcomputer_psd.cc
 * \brief computer power spectral density (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 03/01/2019
 * 
 * computer power spectral density (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 03/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_DPSDCOMPUTER_PSD_CC_VERSION \
  "PSDXX_DPSDCOMPUTER_PSD_CC   V1.0"

#include <psdxx/psd.h>
#include <psdxx/debug.h>

namespace psd {

  /*! compute power spectral density
   *
   * Compute the Fourier transform of
   * \f[
   *   P(t)=\lim_{T\rightarrow\infty}
   *   \frac{1}{2T}\int\limits_{-T}^{T}
   *   x(\tau)\,x(t+\tau)\,\mathrm{d}\tau
   * \f]
   *
   * The function returns are series of real values.
   */
  TDISeries DPSDComputer::psd(const TDISeries::Tcoc& s) const
  {
    PSDXX_debug(this->debug(), "DPSDComputer::psd",
                PSDXX_value(s.interval));
    SpectralValues sv=this->processor(s, s, false, true);
    TDISeries retval(sv.psd1);
    return(retval);
  } // TDISeries DPSDComputer::psd(const TDISeries::Tcoc& s) const

} // namespace psd

/* ----- END OF dpsdcomputer_psd.cc ----- */
