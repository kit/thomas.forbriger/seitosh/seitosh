/*! \file dpsdcomputer_parameters.cc
 * \brief handle parameters for DPSDComputer (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 03/01/2019
 * 
 * handle parameters for DPSDComputer (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 03/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_DPSDCOMPUTER_PARAMETERS_CC_VERSION \
  "PSDXX_DPSDCOMPUTER_PARAMETERS_CC   V1.0"

#include <psdxx/psd.h>
#include <psdxx/error.h>

namespace psd {

  void DPSDComputer::set_nsegments(const unsigned int& n)
  {
    PSDXX_assert(n>0, "number of segments must be finite");
    this->Mnsegments=n;
  } // void DPSDComputer::set_nsegments(const unsigned int& n)

  /* ---------------------------------------------------------------------- */

  void DPSDComputer::set_divisor(const unsigned int& n)
  {
    PSDXX_assert(n>0, "divisor must be finite");
    this->Mdivisor=n;
  } // void DPSDComputer::set_divisor(const unsigned int& n)

  /* ---------------------------------------------------------------------- */

  void DPSDComputer::set_padfactor(const unsigned int& n)
  {
    PSDXX_assert(n>0, "padding factor must be finite");
    this->Mpadfactor=n;
  } // void DPSDComputer::set_padfactor(const unsigned int& n)

  /* ---------------------------------------------------------------------- */

  void DPSDComputer::set_overlap(const double& f)
  {
    PSDXX_assert(f>=0., "overlap fraction must be positive");
    PSDXX_assert(f<=0.5, "overlap fraction is too large");
    this->Moverlap=f;
  } // void DPSDComputer::set_overlap(const double& f)

  /* ---------------------------------------------------------------------- */

  void DPSDComputer::set_verbose(const bool& f)
  {
    this->Mverbose=f;
  } // void DPSDComputer::set_verbose(const bool& f)

  /* ---------------------------------------------------------------------- */

  void DPSDComputer::set_debug(const bool& f)
  {
    this->Mdebug=f;
  } // void DPSDComputer::set_debug(const bool& f)

} // namespace psd

/* ----- END OF dpsdcomputer_parameters.cc ----- */
