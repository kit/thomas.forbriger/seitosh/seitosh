/*! \file function_lin_frequency.cc
 * \brief implementation of function lin_frequency() (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 07/01/2019
 * 
 * implementation of function lin_frequency() (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 07/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_FUNCTION_LIN_FREQUENCY_CC_VERSION \
  "PSDXX_FUNCTION_LIN_FREQUENCY_CC   V1.0"

#include <psdxx/psd.h>
#include <psdxx/error.h>

namespace psd {

   TDseries lin_frequency(const double& df, 
                          const unsigned int& nsamples)
   {
     PSDXX_assert(nsamples>0,
                  "at least one sample must be present");
     TDseries retval(0,nsamples-1);
     for (int i=retval.f(); i<=retval.l(); ++i)
     {
       retval(i)=df*i;
     }
     return(retval);
   } // TDseries lin_frequency(const double& df, 
     //                        const unsigned int& nsamples)


} // namespace psd

/* ----- END OF function_lin_frequency.cc ----- */
