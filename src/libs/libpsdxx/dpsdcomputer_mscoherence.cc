/*! \file dpsdcomputer_mscoherence.cc
 * \brief compute magnitude squared coherence (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 03/01/2019
 * 
 * compute magnitude squared coherence (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 03/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_DPSDCOMPUTER_MScoherence_CC_VERSION \
  "PSDXX_DPSDCOMPUTER_MScoherence_CC   V1.0"

#include <psdxx/psd.h>
#include <aff/iterator.h>
#include <aff/functions/max.h>

namespace psd {

  /*! compute magnitude squared coherence
   *
   * Compute
   * \f[
   *   \frac{\left|P_{lk}\right|^2}{P_{ll}\,P_{kk}},
   * \f]
   * where \f$P_{lk}\f$ is the cross-power spectral density (complex values)
   * of signal \f$k\f$ and signal \f$l\f$.
   * Likewise \f$P_{kk}\f$ and \f$P_{ll}\f$ are the values of power spectral
   * density of signal \f$k\f$ and signal \f$l\f$, respectively.
   *
   * The function returns are series of real values.
   */
  TDISeries DPSDComputer::mscoherence(const TDISeries::Tcoc& s1, 
                                      const TDISeries::Tcoc& s2) const
  {
    SpectralValues sv=this->processor(s1, s2, true, true);
    TDISeries retval=psd::abssqr(sv.cpsd);
    aff::Iterator<TDISeries::Tseries> S(retval.data);
    double psdmax1=aff::func::max(sv.psd1.data);
    double psdmax2=aff::func::max(sv.psd2.data);
    double psdlim=std::sqrt(psdmax1*psdmax2)*1.e-20;
    aff::Browser<TDISeries::Tseries> P1(sv.psd1.data);
    aff::Browser<TDISeries::Tseries> P2(sv.psd2.data);
    while(S.valid() && P1.valid() && P2.valid())
    {
      double psd=std::abs((*P1)*(*P2));
      psd = psd > psdlim? psd : psdlim;
      *S /= psd;
      ++S; ++P1; ++P2;
    }
    return(retval);
  } // TDISeries DPSDComputer::mscoherence(...) const

} // namespace psd

/* ----- END OF dpsdcomputer_mscoherence.cc ----- */
