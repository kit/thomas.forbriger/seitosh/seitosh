/*! \file dpsdcomputer_normalized_cpsd.cc
 * \brief compute normalized cross-power spectral density (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 03/03/2019
 * 
 * compute normalized cross-power spectral density (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 03/03/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_DPSDCOMPUTER_NORMALIZED_CPSD_CC_VERSION \
  "PSDXX_DPSDCOMPUTER_NORMALIZED_CPSD_CC   V1.0"

#include <psdxx/psd.h>
#include <aff/iterator.h>
#include <aff/functions/max.h>

namespace psd {

  /*! compute normalized cross-power spectral density
   *
   * Compute
   * \f[
   *   \frac{P_{lk}}{\sqrt{P_{ll}\,P_{kk}}},
   * \f]
   * where \f$P_{lk}\f$ is the cross-power spectral density (complex values)
   * of signal \f$k\f$ and signal \f$l\f$.
   * Likewise \f$P_{kk}\f$ and \f$P_{ll}\f$ are the values of power spectral
   * density of signal \f$k\f$ and signal \f$l\f$, respectively.
   *
   * The function returns are series of real values.
   */
  TDCISeries DPSDComputer::normalized_cpsd(const TDISeries::Tcoc& s1, 
                                           const TDISeries::Tcoc& s2) const
  {
    SpectralValues sv=this->processor(s1, s2, true, true);
    TDCISeries retval=sv.cpsd;
    aff::Iterator<TDCISeries::Tseries> S(retval.data);
    double psdmax1=aff::func::max(sv.psd1.data);
    double psdmax2=aff::func::max(sv.psd2.data);
    double psdlim=std::sqrt(psdmax1*psdmax2)*1.e-10;
    aff::Browser<TDISeries::Tseries> P1(sv.psd1.data);
    aff::Browser<TDISeries::Tseries> P2(sv.psd2.data);
    while(S.valid() && P1.valid() && P2.valid())
    {
      double psd=std::sqrt((*P1)*(*P2));
      psd = psd > psdlim? psd : psdlim;
      *S /= psd;
      ++S; ++P1; ++P2;
    }
    return(retval);
  } // TDCISeries DPSDComputer::normalized_cpsd(...) const

} // namespace psd

/* ----- END OF dpsdcomputer_normalized_cpsd.cc ----- */
