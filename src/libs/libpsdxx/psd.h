/*! \file psd.h
 * \brief LIBPSDXX - compute (cross) power spectral density (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 01/01/2019
 * 
 * LIBPSDXX - compute (cross) power spectral density (prototypes)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 01/01/2019   V1.0   Thomas Forbriger
 *  - 08/04/2019   V1.1   provide additional version to map complex data
 * 
 * ============================================================================
 */

// include guard
#ifndef PSDXX_PSD_H_VERSION

#define PSDXX_PSD_H_VERSION \
  "PSDXX_PSD_H   V1.1"

#include<complex>
#include<aff/series.h>

/*
 * internal notes
 *
 * DPSDComputer is used to compute a spectral representation of
 * - power spectral density
 * - cross power spectral density
 * - coherence
 *
 * on a uniformly spaced frequency axis. The class provides zero padding,
 * segmentation of time series with overlap and averaging
 *
 * in a subsequent step a second class does sampling on a logarithmic
 * frequency scale
 *
 */

namespace psd {

  /*! Container for series with sampling interval
   */
  template<typename T> 
    struct ConstIntervalSeries
    {
      public:
        //! type of series container
        typedef typename aff::Series<T>::Tcoc Tseries;
        //! type of sample value
        typedef T Tvalue;
      protected:
        aff::Series<Tvalue> Mdata;
      public:
        //! series
        Tseries& data;
        //! sampling interval (seconds for time series, Hz for spectral data)
        double interval;
          
        //! default constructor
        ConstIntervalSeries(): data(Mdata), interval(0) { }
        //! copy constructor
        ConstIntervalSeries(const ConstIntervalSeries& s):
          data(Mdata), interval(s.interval) { Mdata=s.Mdata; }

        //! copy operator
        ConstIntervalSeries& operator=(const ConstIntervalSeries& s)
          { Mdata=s.Mdata; interval=s.interval; return(*this); }
    }; // template<typename T> struct ConstIntervalSeries

  /*! Container for series with sampling interval
   */
  template<typename T> 
    struct IntervalSeries: public ConstIntervalSeries<T>
  {
    public:
      //! type of series container
      typedef ConstIntervalSeries<T> Tbase;
      //! container of constant sample values
      typedef Tbase Tcoc;
      //! type of sample value
      typedef T Tvalue;
      //! type of series container
      typedef aff::Series<Tvalue> Tseries;
    public:
      //! series
      Tseries& data;

      //! default constructor
      IntervalSeries(): data(Tbase::Mdata) { }
      //! copy constructor
      IntervalSeries(const IntervalSeries& s):
        data(Tbase::Mdata) {
          Tbase::Mdata=s.Mdata; 
          Tbase::interval=s.interval; 
        }

      //! copy operator
      IntervalSeries& operator=(const IntervalSeries& s)
        { this->Tbase::operator=(s); return(*this); }
  }; // template<typename T> struct IntervalSeries

  /* ====================================================================== */
  // containers for equally spaced data providing sampling information

  //! time series and real spectral values (PSD and coherence)
  typedef IntervalSeries<double> TDISeries;
  //! complex coefficients
  typedef IntervalSeries<std::complex<double> > TDCISeries;

  /* ====================================================================== */

  //! series container (for frequency values, for example)
  typedef aff::Series<double> TDseries;

  //! series container for complex values
  typedef aff::Series<std::complex<double> > TDCseries;

  /* ====================================================================== */


  /*! \name Functions operating on interval series
   *
   */
  //!@{
  //! return magnitude squared of complex values
  TDISeries abssqr(const TDCISeries::Tcoc& s);

  //! return absolute value (magnitude) of complex values
  TDISeries abs(const TDCISeries::Tcoc& s);

  //! return argument of complex values 
  TDISeries arg(const TDCISeries::Tcoc& s);

  //! return complex conjugate values
  TDCISeries conj(const TDCISeries::Tcoc& s);

  //! return sqrt values
  TDISeries sqrt(const TDISeries::Tcoc& s);
  //!@}

/* ---------------------------------------------------------------------- */

  /*! \name Functions operating on series
   *
   */
  //!@{
  //! return magnitude squared of complex values
  TDseries abssqr(const TDCseries::Tcoc& s);

  //! return absolute value (magnitude) of complex values
  TDseries abs(const TDCseries::Tcoc& s);

  //! return argument of complex values 
  TDseries arg(const TDCseries::Tcoc& s);

  //! return complex conjugate values
  TDCseries conj(const TDCseries::Tcoc& s);

  //! return sqrt values
  TDseries sqrt(const TDseries::Tcoc& s);
  //!@}

  /* ====================================================================== */

  /*! \brief double precision function class for PSD computation
   *
   * This class computes
   * - power spectral density of a single time series
   * - cross power spectral density of two time series
   * - coherence of two time series
   *
   * The actual computation is provided through function operators.
   *
   * Returns epctral values on uniformly spaced frequency axis.
   */
  class DPSDComputer
  {
    public:
      //! default constructor
      DPSDComputer(): 
        Mnsegments(1), Moverlap(0.), Mdivisor(1), Mpadfactor(1),
        Mverbose(false), Mdebug(false), Mdemean(true), Mdetrend(true)  { }

      /*! \name configuration set functions
       */
      //!@{
      void set_nsegments(const unsigned int& n);
      void set_divisor(const unsigned int& n);
      void set_padfactor(const unsigned int& n);
      void set_overlap(const double& f);
      void set_verbose(const bool& f=true);
      void set_debug(const bool& f=true);
      //!@}

      /*! \name configuration query functions
       */
      //!@{
      unsigned int nsegments() const { return(Mnsegments); }
      unsigned int divisor() const { return(Mdivisor); }
      unsigned int padfactor() const { return(Mpadfactor); }
      double overlap() const { return(Moverlap); }
      bool verbose() const { return(Mverbose); }
      bool debug() const { return(Mdebug); }
      //!@}

      /*! \name functions computing spectral results
       */
      //!@{
      //! compute power spectral density
      TDISeries psd(const TDISeries::Tcoc& s) const;
      //! compute cross power spectrum
      TDCISeries cross_psd(const TDISeries::Tcoc& s1,
                           const TDISeries::Tcoc& s2) const;
      //! compute normalized cross-power spectral density
      TDCISeries normalized_cpsd(const TDISeries::Tcoc& s1, 
                                 const TDISeries::Tcoc& s2) const;
      //! compute magnitude squared coherence
      TDISeries mscoherence(const TDISeries::Tcoc& s1, 
                            const TDISeries::Tcoc& s2) const;
      //!@}

    private:
      //! number of segments split input time series
      unsigned int Mnsegments;
      //! fraction to let segments of time series overlap
      double Moverlap;
      //! integer factor in number of samples
      unsigned int Mdivisor;
      //! factor to increase size of time series by zero-padding
      unsigned int Mpadfactor;
      //! produce verbose output on terminal
      bool Mverbose;
      //! produce debug output on terminal
      bool Mdebug;
      //! remove average from each segment
      bool Mdemean;
      //! remove trend from each segment
      bool Mdetrend;

      //! container for return value of general processor (internal use only)
      struct SpectralValues
      {
        TDISeries psd1, psd2;
        TDCISeries cpsd;
      }; // struct SpectralValues

      //! general processor (internal use only)
      SpectralValues processor(const TDISeries::Tcoc& s1,
                               const TDISeries::Tcoc& s2,
                               const bool& cpsd_flag,
                               const bool& psd_flag) const;
  }; // class DPSDComputer

  /* ====================================================================== */

   /*! return a linear frequency scale
    *
    * \param df sampling interval in Hz of uniformly samples spectral values
    * \param nsamples number of samples of spectral data
    * \return series of frequency values in Hz with linear spacing with \p df
    *         interval beginning with 0 Hz
    *
    * This function is a corresponding function to log_frequency().
    */
   TDseries lin_frequency(const double& df, 
                          const unsigned int& nsamples);

   /*! \brief return a logarithmic frequency scale
    *
    * \param df sampling interval in Hz of uniformly samples spectral values
    * \param nsamples number of samples of spectral data
    * \param n_per_decade number of samples per frequency decade
    *                     for logarithmic sampling
    * \return series of frequency values in Hz with logarithmic spacing and 
    *         \p n_per_decade frequency values per decade
    *
    * This function is used to compute a series of frequency values with
    * logarithmic spacing. The return value can be used as input parameter to
    * the actual sampling functions.
    */
   TDseries log_frequency(const double& df, 
                          const unsigned int& nsamples,
                          const unsigned int& n_per_decade);

   /*! \brief return a logarithmic frequency scale
    *
    * \param lin_frequency series of frequency values on linear scale
    * \param n_per_decade number of samples per frequency decade
    *                     for logarithmic sampling
    * \return series of frequency values in Hz with logarithmic spacing and 
    *         \p n_per_decade frequency values per decade
    *
    * This function is used to compute a series of frequency values with
    * logarithmic spacing. The return value can be used as input parameter to
    * the actual sampling functions.
    */
   TDseries log_frequency(const TDseries::Tcoc& lin_frequency,
                          const unsigned int& n_per_decade);

   /*! sample complex values on a logarithmic frequency axis
    *
    * \param s spectral values with uniform sampling along frequency axis
    * \param f frequency values on a logarithmic scale like returned by
    *          function log_frequency()
    * \return series of sampled spectral values, where each sample of the
    *         returned series corresponds to the frequency with the
    *         corresponding index in \p f
    */
   TDCISeries::Tseries log_sampling(const TDCISeries::Tcoc& s,
                                    const TDseries::Tcoc& f);

   /*! sample real values on a logarithmic frequency axis
    *
    * \param s spectral values with uniform sampling along frequency axis
    * \param f frequency values on a logarithmic scale like returned by
    *          function log_frequency()
    * \return series of sampled spectral values, where each sample of the
    *         returned series corresponds to the frequency with the
    *         corresponding index in \p f
    */
   TDISeries::Tseries log_sampling(const TDISeries::Tcoc& s,
                                   const TDseries::Tcoc& f);

   /*! sample real values on a logarithmic frequency axis
    *
    * \param s spectral values with uniform sampling along frequency axis
    * \param flin frequency values on a linear scale, matching the input
    *             series \p s
    * \param flog frequency values on a logarithmic scale like returned by
    *          function log_frequency()
    * \return series of sampled spectral values, where each sample of the
    *         returned series corresponds to the frequency with the
    *         corresponding index in \p flog
    */
   TDseries log_sampling(const TDseries::Tcoc& s,
                         const TDseries::Tcoc& flin,
                         const TDseries::Tcoc& flog);

   /*! sample complex values on a logarithmic frequency axis
    *
    * \param s spectral values with uniform sampling along frequency axis
    * \param flin frequency values on a linear scale, matching the input
    *             series \p s
    * \param flog frequency values on a logarithmic scale like returned by
    *          function log_frequency()
    * \return series of sampled spectral values, where each sample of the
    *         returned series corresponds to the frequency with the
    *         corresponding index in \p flog
    */
   TDCseries log_sampling(const TDCseries::Tcoc& s,
                          const TDseries::Tcoc& flin,
                          const TDseries::Tcoc& flog);

} // namespace psd

#endif // PSDXX_PSD_H_VERSION (includeguard)

/* ----- END OF psd.h ----- */
