/*! \file helper.h
 * \brief utility functions and classes, to be used internally (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 12/01/2019
 * 
 * utility functions and classes, to be used internally (prototypes)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 12/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */

// include guard
#ifndef PSDXX_HELPER_H_VERSION

#define PSDXX_HELPER_H_VERSION \
  "PSDXX_HELPER_H   V1.0"

#include <psdxx/psd.h>

namespace psd {

  namespace helper {

    /*! \brief Function class to return index on logarithmic scale
     *
     * Support inverse operation to map index to lograrithmic frequency axis.
     * The returned index value may be fractional, it is not an integer.
     *
     * Consider a series
     * \f[
     * f_l = a\, c^{l} + b
     * \f]
     * of frequency values on a logarithmic scale.
     * Then
     * \f[
     * f_0 = a + b
     * \f]
     * \f[
     * f_1 = a\, c + b,
     * \f]
     * and
     * \f[
     * f_2 = a\, c^{2} + b.
     * \f]
     * Hence
     * \f[
     * f_1 - f_0 = a\,(c-1)
     * \f]
     * and
     * \f[
     * f_2 - f_1 = a\,c\,(c-1)
     * \f]
     * such that
     * \f[
     * c=\frac{f_2-f_1}{f_1-f_0},
     * \f]
     * \f[
     * a=\frac{f_1-f_0}{c-1},
     * \f]
     * and
     * \f[
     * b=f_0-a.
     * \f]
     * The index value of a given frequency \f$ f \f$ then is
     * \f[
     * l=\log_{c}\frac{f-b}{a}=\frac{\ln\left(\frac{f-b}{a}\right)}{\ln(c)}
     * \f]
     */
    class LogIndex 
    {
      public:
        /*! \brief Constructor.
         * Initialize class with any three consecutive values of frequency on
         * a logaritmic scale. The LogIndex::operator()() function will return
         * 0 for \p f0.
         */
        LogIndex(const double& f0,
                 const double& f1,
                 const double& f2);
        /*! \brief Return index value for given frequency.
         * Returns 0 for \p f0, 1 for \p f1, and 2 for \p f2, where \p f0, \p
         * f1, and \p f2 are the arguments passed to the constructor.
         *
         * \param f frequency
         * \return fractional index of frequency \p f
         */
        double operator()(const double& f) const;
      private:
        //! factor in front of exponential function in mapping function
        double Ma;
        //! constant in mapping function
        double Mb;
        //! logarithm of base of exponential function
        double Mlnc;
    }; // class LogIndex

/* ---------------------------------------------------------------------- */

    /*! \brief Taper function class
     */
    class LogTaper 
    {
      private:
        /*! Indicate location of center of taper
         */
        enum Eedge {
          Fmiddle=   (1<<0), //!< between smallest and largest frequency index
          Fupper_end=(1<<1), //!< smallest frequency index
          Flower_end=(1<<2), //!< largest frequency index
        }; // enum Eedge
      public:
        LogTaper(const psd::TDseries::Tcoc& f,
              const unsigned int& i);
        ~LogTaper();
        double operator()(const double& f);
        double sum() const { return(Msum); }
      private:
        double Msum;
        LogIndex* Mli;
        Eedge Mloc;
    }; // class LogTaper

  } // namespace helper

} // namespace psd

#endif // PSDXX_HELPER_H_VERSION (includeguard)

/* ----- END OF helper.h ----- */
