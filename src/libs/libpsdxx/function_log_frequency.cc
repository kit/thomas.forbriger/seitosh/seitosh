/*! \file function_log_frequency.cc
 * \brief implementation of function log_frequency() (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 07/01/2019
 * 
 * implementation of function log_frequency() (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 07/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_FUNCTION_LOG_FREQUENCY_CC_VERSION \
  "PSDXX_FUNCTION_LOG_FREQUENCY_CC   V1.0"

#include <psdxx/psd.h>
#include <psdxx/error.h>

namespace psd {

   /*! \brief return a logarithmic frequency scale
    *
    * \par mapping function
    * The series of frequencies with logarithmic spacing is
    * \f[
    * f_l = \Delta f \, \left\{ c \, \left(10^{\frac{l}{m}}-1 \right) +1
    * \right\}
    * \f]
    * where 
    * - \f$ \Delta f =\f$ \p df
    * - \f$ m= \f$ \p n_per_decade
    * .
    * This mapping ensures \f$ f_0 = \Delta f \f$.
    * Adjust \f$ c \f$ such that
    * \f[
    * f_1 - f_0 \geq \Delta f.
    * \f]
    * This condition is satisfied for
    * \f[
    *   c = \textrm{max}\left(1,\frac{1}{10^{\frac{1}{m}}-1}\right)
    * \f]
    * The largest index \f$ l_\textrm{max} \f$ of the output array
    * is
    * \f[
    * l_\textrm{max} = \textrm{floor}\left\{
    * m\,\log_{10}\left(\frac{\frac{f_\textrm{max}}{\Delta f}-1}{c}+1
    * \right)\right\}
    * \f]
    * to make
    * \f[
    * f(l_\textrm{max}) \leq N\,\Delta f
    * \f]
    * and 
    * \f[
    * N\,\Delta f - f(l_\textrm{max}) \leq \Delta f
    * \f]
    * if \f$ N= \f$ \p nsamples.
    *
    * \par coefficients obtained from frequency values
    * \f[
    * f_0 = \Delta f
    * \f]
    * \f[
    * f_1 = \Delta f \, c\, 10^{\frac{1}{m}} - \Delta f \, c + \Delta f
    * \f]
    * \f[
    * f_2 = \Delta f \, c\, 10^{\frac{1}{m}}\, 10^{\frac{1}{m}}
    *     - \Delta f \, c + \Delta f
    * \f]
    * Hence
    * \f[
    * \Delta f = f_0
    * \f]
    * \f[
    * 10^{\frac{1}{m}} = \frac{f_2-f_1}{f_1-f_0}
    * \f]
    * \f[
    * c = \frac{f_1-f_0}{f_0\,\left(\frac{f_2-f_1}{f_1-f_0}-1\right)}
    * \f]
    */
   TDseries log_frequency(const double& df, 
                          const unsigned int& nsamples,
                          const unsigned int& n_per_decade)
   {
     PSDXX_assert(n_per_decade>0, "must be positive and finite");
     PSDXX_assert(nsamples>0, "must be positive and finite");
     PSDXX_assert(df>0, "must be positive and finite");
     double logfac=std::pow(10.,1./n_per_decade);
     double c=1./(logfac-1.);
     c=c<1.? 1. : c;
     unsigned int lmax
       =int(std::floor(n_per_decade
                       *std::log(1.+(nsamples-1.)/c)/std::log(10.)));
     TDseries retval(0,lmax);
     for (int i=retval.f(); i<=retval.l(); ++i)
     {
       retval(i)=df*(c*(std::pow(logfac,i)-1.)+1.);
     }
     return(retval);
   } // TDseries log_frequency(const double& df, 
     //                        const unsigned int& nsamples,
     //                        const unsigned int& n_per_decade)

 /* ---------------------------------------------------------------------- */

   TDseries log_frequency(const TDseries::Tcoc& flin,
                          const unsigned int& n_per_decade)
   {
    // consistency checks
    PSDXX_assert(flin.size()>1, 
                 "input series must contain at least 2 values");
    double df1=flin(flin.f()+1)-flin(flin.f());
    double df2=flin(flin.l())-flin(flin.l()-1);
    PSDXX_assert(std::abs((df2/df1)-1.)<1.e-8,
                 "input frequency scale is not linear");
    return(log_frequency(df1, flin.size(), n_per_decade));
   } // TDseries log_frequency(const TDseries::Tcoc& lin_frequency,
     //                        const unsigned int& n_per_decade)

} // namespace psd

/* ----- END OF function_log_frequency.cc ----- */
