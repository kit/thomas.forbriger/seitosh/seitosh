/*! \file dpsdcomputer_cross_psd.cc
 * \brief compute cross power spectral density (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 03/01/2019
 * 
 * compute cross power spectral density (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 03/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_DPSDCOMPUTER_CROSS_PSD_CC_VERSION \
  "PSDXX_DPSDCOMPUTER_CROSS_PSD_CC   V1.0"

#include <psdxx/psd.h>

namespace psd {

  /*! compute cross-power spectral density
   *
   * Compute the Fourier transform of
   * \f[
   *   P_{xy}(t)=\lim_{T\rightarrow\infty}
   *   \frac{1}{2T}\int\limits_{-T}^{T}
   *   x(\tau)\,y(t+\tau)\,\mathrm{d}\tau
   * \f]
   *
   * The function returns are series of complex values.
   */
  TDCISeries 
    DPSDComputer::cross_psd(const TDISeries::Tcoc& s1,
                            const TDISeries::Tcoc& s2) const
  {
    SpectralValues sv=this->processor(s1, s2, true, false);
    TDCISeries retval(sv.cpsd);
    return(retval);
  } // TDCISeries DPSDComputer::cross_psd(...) const

} // namespace psd

/* ----- END OF dpsdcomputer_cross_psd.cc ----- */
