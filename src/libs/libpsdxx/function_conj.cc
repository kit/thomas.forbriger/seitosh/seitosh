/*! \file function_conj.cc
 * \brief return series of complex conjugate values (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 02/02/2019
 * 
 * return series of complex conjugate values (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 02/02/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_FUNCTION_CONJ_CC_VERSION \
  "PSDXX_FUNCTION_CONJ_CC   V1.0"

#include <psdxx/psd.h>
#include <aff/iterator.h>

namespace psd {

  TDCseries conj(const TDCseries::Tcoc& s)
  {
    TDCseries retval(s.shape());

    aff::Iterator<TDCseries> S(retval);
    aff::Browser<TDCseries::Tcoc> C(s);

    while(S.valid() && C.valid())
    {
      *S = std::conj(*C);
      ++S; ++C;
    }
    return(retval);
  } // TDCseries conj(const TDCseries::Tcoc& s)

  /* ---------------------------------------------------------------------- */

  TDCISeries conj(const TDCISeries::Tcoc& s)
  {
    TDCISeries retval;
    retval.interval=s.interval;
    retval.data=conj(s.data);
    return(retval);
  } // TDCISeries conj(const TDCISeries::Tcoc& s)

} // namespace psd

/* ----- END OF function_conj.cc ----- */
