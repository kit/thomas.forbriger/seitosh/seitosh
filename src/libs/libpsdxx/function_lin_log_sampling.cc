/*! \file function_lin_log_sampling.cc
 * \brief take a series with linear sampling and resample to logarithmic sampling (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 14/02/2019
 * 
 * take a series with linear sampling and resample to logarithmic sampling
 * (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 14/02/2019   V1.0   Thomas Forbriger
 *  - 08/04/2019   V1.1   provide version for complex data too
 * 
 * ============================================================================
 */
#define PSDXX_FUNCTION_LIN_LOG_SAMPLING_CC_VERSION \
  "PSDXX_FUNCTION_LIN_LOG_SAMPLING_CC   V1.1"

#include <psdxx/psd.h>
#include <psdxx/error.h>

namespace psd {

  TDseries log_sampling(const TDseries::Tcoc& s,
                        const TDseries::Tcoc& flin,
                        const TDseries::Tcoc& flog)
  {
    // consistency checks
    PSDXX_assert(s.size()>1, "input series must contain at least 2 values");
    PSDXX_assert(s.size()==flin.size(),
                 "series of frequencies does not match data series");
    double df1=flin(flin.f()+1)-flin(flin.f());
    double df2=flin(flin.l())-flin(flin.l()-1);
    PSDXX_assert(std::abs((df2/df1)-1.)<1.e-8,
                 "input frequency scale is not linear");
    TDISeries::Tcoc data;
    data.interval=df1;
    data.data=s;
    TDseries retval=log_sampling(data, flog);
    return(retval);
  } // TDseries log_sampling(const TDseries::Tcoc& s,
    //                       const TDseries::Tcoc& flin,
    //                       const TDseries::Tcoc& flog)

  /* ---------------------------------------------------------------------- */

  TDCseries log_sampling(const TDCseries::Tcoc& s,
                         const TDseries::Tcoc& flin,
                         const TDseries::Tcoc& flog)
  {
    // consistency checks
    PSDXX_assert(s.size()>1, "input series must contain at least 2 values");
    PSDXX_assert(s.size()==flin.size(),
                 "series of frequencies does not match data series");
    double df1=flin(flin.f()+1)-flin(flin.f());
    double df2=flin(flin.l())-flin(flin.l()-1);
    PSDXX_assert(std::abs((df2/df1)-1.)<1.e-8,
                 "input frequency scale is not linear");
    TDCISeries::Tcoc data;
    data.interval=df1;
    data.data=s;
    TDCseries retval=log_sampling(data, flog);
    return(retval);
  } // TDCseries log_sampling(const TDCseries::Tcoc& s,
    //                        const TDseries::Tcoc& flin,
    //                        const TDseries::Tcoc& flog)

} // namespace psd

/* ----- END OF function_lin_log_sampling.cc ----- */
