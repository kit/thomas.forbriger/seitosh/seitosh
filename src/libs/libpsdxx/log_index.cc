/*! \file log_index.cc
 * \brief compute index on logarithmic scale (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 12/01/2019
 * 
 * compute index on logarithmic scale (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 12/01/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */
#define PSDXX_LOG_INDEX_CC_VERSION \
  "PSDXX_LOG_INDEX_CC   V1.0"

#include <psdxx/helper.h>
#include <psdxx/error.h>

namespace psd {

  namespace helper {

    LogIndex::LogIndex(const double& f0,
                       const double& f1,
                       const double& f2)
    {
      PSDXX_assert(f1>f0, "frequency values must increase");
      PSDXX_assert(f2>f1, "frequency values must increase");
      double c=(f2-f1)/(f1-f0);
      Ma=(f1-f0)/(c-1.);
      Mb=f0-Ma;
      Mlnc=std::log(c);
    } // LogIndex::LogIndex(const double& f0,
      //                    const double& f1,
      //                    const double& f2)


  /* ---------------------------------------------------------------------- */
   
    double LogIndex::operator()(const double& f) const
    {
     double l = std::log((f-Mb)/Ma)/Mlnc;
     return l;
    } // double LogIndex::operator()(const double& f)

  } // namespace helper

} // namespace psd

/* ----- END OF log_index.cc ----- */
