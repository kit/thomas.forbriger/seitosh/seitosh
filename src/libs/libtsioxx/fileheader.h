/*! \file fileheader.h
 * \brief define ts::sff::FileHeader (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 02/04/2019
 * 
 * define ts::sff::FileHeader (prototypes)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * libtsioxx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 02/04/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */

// include guard
#ifndef TSIO_FILEHEADER_H_VERSION

#define TSIO_FILEHEADER_H_VERSION \
  "TSIO_FILEHEADER_H   2019/04/02"

#include<sffxx.h>

namespace ts {

  namespace sff {

    /*! \brief hold information for a complete SFF file header
     * \ingroup group_sfftsfile
     */
    class FileHeader {
      public:
        FileHeader(): Mhasfree(false), Mhassrce(false) { }
        void srce(const ::sff::SRCE& s);
        void free(const ::sff::FREE& f);
        void append(const ::sff::FREE& f);
        ::sff::FREE free() const { return Mfree; }
        ::sff::SRCE srce() const { return Msrce; }
        bool hasfree() const { return Mhasfree; }
        bool hassrce() const { return Mhassrce; }
      private:
        ::sff::FREE Mfree;
        ::sff::SRCE Msrce;
        bool Mhasfree;
        bool Mhassrce;
    }; // struct FileHeader

  } // namespace sff

} // namespace ts

#endif // TSIO_FILEHEADER_H_VERSION (includeguard)

/* ----- END OF fileheader.h ----- */
