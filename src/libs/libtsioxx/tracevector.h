/*! \file tracevector.h
 * \brief define ts::sff::TraceVector<C> (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 02/04/2019
 * 
 * define ts::sff::TraceVector<C> (prototypes)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * libtsioxx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 02/04/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */

// include guard
#ifndef TSIO_TRACEVECTOR_H_VERSION

#define TSIO_TRACEVECTOR_H_VERSION \
  "TSIO_TRACEVECTOR_H   2019/04/02"

#include<vector>
#include<tsioxx/sfftimeseries.h>

namespace ts {

  namespace sff {

    /*! \brief hold SFF traces with full header information
     * \ingroup group_sfftsfile 
     */
    template<class C>
      class TraceVector: public std::vector<SFFTimeSeries<C> > {
        public:
          typedef std::vector<SFFTimeSeries<C> > Tbase;
          typedef SFFTimeSeries<C> Ttimeseries;
      }; // class TraceVector

  } // namespace sff

} // namespace ts

#endif // TSIO_TRACEVECTOR_H_VERSION (includeguard)

/* ----- END OF tracevector.h ----- */
