/*! \file sfftsfileread.h
 * \brief define ts::sff::File<C>::read function (implementation)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 02/04/2019
 * 
 * define ts::sff::File<C>::read function (implementation)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * libtsioxx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 *  The code in this file must be kept in its own header file, because this
 *  compilation units has to load inputoperators.h which in turn loads
 *  sfftsfile.h
 *
 * REVISIONS and CHANGES 
 *  - 02/04/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */

// include guard
#ifndef TSIO_SFFTSFILEREAD_H_VERSION

#define TSIO_SFFTSFILEREAD_H_VERSION \
  "TSIO_SFFTSFILEREAD_H   2019/04/02"

#include<tsioxx/sfftsfile.h>
#include<tsioxx/inputoperators.h>

namespace ts {

  namespace sff {

    /*======================================================================*/
    // member functions
    
    /*! \brief Read a complete file with all traces.
     */
    template<class C>
    inline void File<C>::read(datrw::idatstream& is, const bool& verbose)
    {
      Trangelist rl;
      this->read(is, rl, verbose);
    } // File<C>::read
    
    /*----------------------------------------------------------------------*/

    
    /*! \brief Read a data file an extract selected traces.
     */
    template<class C>
      inline void File<C>::read(datrw::idatstream& is, 
                                const Trangelist& rl,
                                const bool& verbose)
    {
      is >> this->fileheader;
      int itrace=0;
      while (is.good())
      {
        ++itrace;
        if ((rl.size()==0) || rl.contains(itrace))
        {
          if (verbose)
          { std::cout << "  * read trace #" << itrace << std::endl; }
          Ttimeseries timeseries;
          is >> timeseries;
          timeseries.settraceindex(itrace);
          this->push_back(timeseries);
        }
        else
        {
          if (verbose)
          { std::cout << "    skip trace #" << itrace << std::endl; }
          is.skipseries();
        }
      }
    } // File<C>::read

  } // namespace sff

} // namespace ts

#endif // TSIO_SFFTSFILEREAD_H_VERSION (includeguard)

/* ----- END OF sfftsfileread.h ----- */
