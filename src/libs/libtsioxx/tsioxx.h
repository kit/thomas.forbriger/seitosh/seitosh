/*! \file tsioxx.h
 * \brief load all modules (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 02/04/2019
 * 
 * load all modules (prototypes)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * libtsioxx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 02/04/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */

// include guard
#ifndef TSIO_TSIOXX_H_VERSION

#define TSIO_TSIOXX_H_VERSION \
  "TSIO_TSIOXX_H   2019/04/02"

#include<tsioxx/sfftsfileread.h>
#include<tsioxx/operators.h>
#include<tsioxx/cmdlinefiles.h>

/*! \brief Outer namespace
 *
 * All modules are placed within this namespace, which is the namespace of
 * modules from libtsxx.
 */
namespace ts {

  /*! \brief Namespace for libtsioxx modules
   *
   * To distinguish the modules in libtsioxx and to avoid future conflicts,
   * all modules in libtsioxx are placed in this namespace.
   */
  namespace sff {

  } // namespace sff

} // namespace ts

#endif // TSIO_TSIOXX_H_VERSION (includeguard)

/* ----- END OF tsioxx.h ----- */
