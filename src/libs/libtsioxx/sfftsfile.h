/*! \file sfftsfile.h
 * \brief data file container for SFF TimeSeries (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \since 18/07/2005
 * \date 30/01/2014
 * 
 * data file container for SFF TimeSeries (prototypes)
 * 
 * Copyright (c) 2005-2007, 2012, 2014 by Thomas Forbriger (BFO Schiltach) 
 *
 * ----
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 30/01/2014   V1.0   Thomas Forbriger (thof):
 *                        copied from sffheaders.h
 *  - 02/04/2019   V1.1   just define ts::sff::File<C>
 * 
 * ============================================================================
 */

// include guard
#ifndef TSIO_SFFTSFILE_H_VERSION

#define TSIO_SFFTSFILE_H_VERSION \
  "TF_SFFTSFILE_H   2019/04/02"

#include<tsioxx/fileheader.h>
#include<tsioxx/tracevector.h>
#include<tfxx/rangelist.h>
#include<datrwxx/datread.h>

namespace ts {

  namespace sff {

    /*! \brief SFF File class
     * \defgroup group_sfftsfile SFF File class
     *
     * This module is presented through sfftsfile.h
     *
     * @{
     */

    /*! \brief hold all information contained in an SFF data file
     *
     * Store the complete information contained in a data file
     */
    template<class C>
      struct File: public TraceVector<C> {
        public:
          typedef TraceVector<C> Tbase;
          typedef Tbase Ttracevector;
          typedef typename Tbase::Ttimeseries Ttimeseries;
          typedef tfxx::RangeList<int> Trangelist;
          void read(datrw::idatstream& is, 
                    const bool& verbose=false);
          void read(datrw::idatstream& is, 
                    const Trangelist& rl,
                    const bool& verbose=false);
          FileHeader fileheader;
      }; // struct File

    /*!
     * @}
     */
    

  } // namespace sff
} // namespace ts
    
#endif // TSIO_SFFTSFILE_H_VERSION (includeguard)

/* ----- END OF sfftsfile.h ----- */
