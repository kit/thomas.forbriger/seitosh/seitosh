/*! \file traceheader.h
 * \brief define ts::sff::TraceHeader (prototypes)
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 02/04/2019
 * 
 * define ts::sff::TraceHeader (prototypes)
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * ----
 * libtsioxx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version. 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * ----
 *
 * REVISIONS and CHANGES 
 *  - 02/04/2019   V1.0   Thomas Forbriger
 * 
 * ============================================================================
 */

// include guard
#ifndef TSIO_TRACEHEADER_H_VERSION

#define TSIO_TRACEHEADER_H_VERSION \
  "TSIO_TRACEHEADER_H   2019/04/02"

#include<sffxx.h>

namespace ts {

  namespace sff {

    /*! \brief hold information for a complete SFF trace header
     * \ingroup group_sfftimeseries
     */
    class TraceHeader {
      public:
        TraceHeader(): Mhasfree(false), Mhasinfo(false) { }
        void info(const ::sff::INFO& s);
        void free(const ::sff::FREE& f);
        void wid2(const ::sff::WID2& w) { Mwid2=w; }
        void append(const ::sff::FREE& f);
        ::sff::FREE free() const { return Mfree; }
        ::sff::INFO info() const { return Minfo; }
        ::sff::WID2 wid2() const { return Mwid2; }
        bool hasfree() const { return Mhasfree; }
        bool hasinfo() const { return Mhasinfo; }
        void read(std::istream& is, const bool& verbose=false);
      private:
        ::sff::FREE Mfree;
        ::sff::INFO Minfo;
        ::sff::WID2 Mwid2;
        bool Mhasfree;
        bool Mhasinfo;
    }; // class TraceHeader

  } // namespace sff

} // namespace ts

#endif // TSIO_TRACEHEADER_H_VERSION (includeguard)

/* ----- END OF traceheader.h ----- */
