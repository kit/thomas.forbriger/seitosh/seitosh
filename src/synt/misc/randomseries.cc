/*! \file randomseries.cc
 * \brief provide time series of incoherent random numbers
 * 
 * ----------------------------------------------------------------------------
 * 
 * \author Thomas Forbriger
 * \date 31/10/2019
 * 
 * provide time series of incoherent random numbers
 * 
 * Copyright (c) 2019 by Thomas Forbriger (BFO Schiltach) 
 * 
 * REVISIONS and CHANGES 
 *  - 31/10/2019   V1.0   Thomas Forbriger
 *  - 02/10/2020   V1.1   pass seed value to rng
 * 
 * ============================================================================
 */
#define RANDOMSERIES_VERSION \
  "RANDOMSERIES   V1.1   provide time series of incoherent random numbers"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <tfxx/commandline.h>
#include <tfxx/seitosh.h>
#include <tfxx/error.h>
#include <tfxx/rng.h>
#include <aff/series.h>
#include <aff/iterator.h>
#include <datrwxx/writeany.h>

using std::cout;
using std::cerr;
using std::endl;

/* ---------------------------------------------------------------------- */

struct Options {
  bool verbose, overwrite, setseed;
  std::string rngtype, filename, filetype;
  int nseries, nsamples;
  double std, mean, dt;
  long unsigned int seed;
}; // struct Options

/* ---------------------------------------------------------------------- */

int main(int iargc, char* argv[])
{

  // define usage information
  char usage_text[]=
  {
    RANDOMSERIES_VERSION "\n"
    "usage: randomseries filename [-v] [-rngtype t] [-t type] [-o]" "\n"
    "                    [-nseries n] [-nsamples n] [-dt v]" "\n"
    "                    [-std v] [-mean v] [-seed n]" "\n"
    "                    [-rngtype rng]" "\n"
    "   or: randomseries --help|-h" "\n"
    "   or: randomseries --xhelp[=type]\n"
  };

  // define full help text
  char help_text[]=
  {
    "-help		print description\n"
    "-xhelp[=type] print detailed information\n"
    "             if 'type' is not specfied, or equals 'all', print\n"
    "               information regarding all supported file formats\n"
    "             if 'type' is specified, just print text for file\n"
    "               format 'type'\n"
    "             if 'type' is 'gslrng' print recommendations regarding\n"
    "               random number generator type\n"
    "\n"
    "filename     name of output file\n"
    "-v           be verbose\n"
    "-o           overwrite existing output file\n"
    "-rngtype t   select random number generator type\n"
    "             run\n"
    "               randomseries -xhelp=gslrng\n"
    "             to see comments\n"
    "-t type      select output file type\n"
    "-nseries n   produce 'n' time series\n"
    "-nsamples n  produce 'n' samples per series\n"
    "-dt v        set sampling interval to 'v' seconds\n"
    "-rngtype rng select random number generator\n"
    "-seed n      initialize random number generator with 'n'\n"
    "-std v       set standard deviation to 'v'\n"
    "-mean v      set mean value of samples to 'v'\n"
    "\n"
    "The program uses the GSL (GNU Scientific Library) random number\n"
    "generators. See https://www.gnu.org/software/gsl/doc/html/rng.html\n"
    "\n"
    "The programs main purpose is to provide several time series\n"
    "which are incoherent (as far as possible). All samples are taken\n"
    "sequentially from the random number generator with the generator\n"
    "being seeded only at the very beginning. If several (incoherent)\n"
    "time series should be computed by individual calls to a random\n"
    "number generator program, this would require careful seeding\n"
    "for each program invocation to make sure that different sequences\n"
    "of samples are indeed independent or at least incoherent.\n"
    "If no seed value is passed to the program, the random number generator\n"
    "is seeded with the current system clock count.\n"
  };

  // define commandline options
  using namespace tfxx::cmdline;
  static Declare options[]= 
  {
    // 0: print help
    {"help",arg_no,"-"},
    // 1: verbose mode
    {"v",arg_no,"-"},
    // 2: overwrite existing output file
    {"o",arg_no,"-"},
    // 3: select output file type
    {"t",arg_yes,"sff"},
    // 4: select output file type
    {"nseries",arg_yes,"1"},
    // 5: select output file type
    {"nsamples",arg_yes,"10000"},
    // 6: select output file type
    {"seed",arg_yes,"0"},
    // 7: select output file type
    {"std",arg_yes,"1."},
    // 8: select output file type
    {"mean",arg_yes,"0."},
    // 9: output file format
    {"xhelp",arg_opt,"all"},
    // 10: output file format
    {"rngtype",arg_yes,"default"},
    // 11: sampling interval
    {"dt",arg_yes,"1."},
    {NULL}
  };

  // no arguments? print usage...
  if (iargc<2) 
  {
    cerr << usage_text << endl;
    cerr << endl << tfxx::seitosh::repository_reference << endl;
    exit(0);
  }

  // collect options from commandline
  Commandline cmdline(iargc, argv, options);

  // help requested? print full help text...
  if (cmdline.optset(0))
  {
    cerr << usage_text << endl;
    cerr << help_text << endl;
    cerr << endl;
    cerr << "supported output data file types:" << endl;
    datrw::supported_output_data_types(cerr);
    cerr << endl << tfxx::seitosh::repository_reference << endl;
    exit(0);
  }

  // help on file format details requested? 
  if (cmdline.optset(9))
  {
    cerr << usage_text << endl;
    cerr << endl;
    if (cmdline.string_arg(9) == "all")
    {
      datrw::online_help(cerr);
    }
    else if (cmdline.string_arg(9) == "gslrng")
    {
      cerr << tfxx::numeric::RNGgaussian::comment_gsl_rng_ranlux << endl;
      cerr << endl;
      cerr << "Available random number generator types:" << endl;
      tfxx::numeric::RNGgaussian::rng_list_types(cerr);
    }
    else
    {
      datrw::online_help(cmdline.string_arg(9), cerr);
    }
    exit(0);
  }

  TFXX_assert(cmdline.extra(), "Missing file name");
  
  // extract command line options
  Options opt;

  opt.filename=cmdline.next();
  opt.verbose=cmdline.optset(1);
  opt.overwrite=cmdline.optset(2);
  opt.filetype=cmdline.string_arg(3);
  opt.nseries=cmdline.int_arg(4);
  opt.nsamples=cmdline.int_arg(5);
  opt.seed=cmdline.int_arg(6);
  opt.setseed=cmdline.optset(6);
  opt.std=cmdline.double_arg(7);
  opt.mean=cmdline.double_arg(8);
  opt.rngtype=cmdline.string_arg(10);
  opt.dt=cmdline.double_arg(11);

  // check parameters
  TFXX_assert(opt.nseries>0, "number of series must be positive");
  TFXX_assert(opt.nsamples>0, "number of samples must be positive");
  TFXX_assert(opt.seed>=0, "seed value must be non-negative");
  TFXX_assert(opt.dt>0, "sampling interval must be positive");

  if (opt.verbose)
  {
    cout << RANDOMSERIES_VERSION << endl;
  }

  // open output file
  if (opt.verbose)
  {
    cout << "open output file " << opt.filename
      << " with format " << opt.filetype << endl;
  }
  if (!opt.overwrite) { datrw::abort_if_exists(opt.filename); }
  std::ofstream ofs(opt.filename.c_str(),
                    datrw::oanystream::openmode(opt.filetype));
  datrw::oanystream os(ofs, opt.filetype);

  // create random number generator
  tfxx::numeric::RNGgaussian rng(opt.std, opt.mean, opt.rngtype.c_str());
  if (opt.setseed) { rng.set(opt.seed); } else { rng.set(); }
  
  if (opt.verbose)
  {
    cout << "RNG parameters:\n"
      << " type: " << rng.type() << "\n"
      << " seed: " << rng.seed() << "\n"
      << " mean: " << rng.mean() << "\n"
      << " std:  " << rng.std() << endl;
    cout << "produce " << opt.nseries << " traces with "
      << opt.nsamples << " samples each" << endl;
    cout << "set sampling interval to " << opt.dt << " seconds" << endl;
  }

  sff::WID2 wid2;
  wid2.nsamples=opt.nsamples;
  wid2.instype=opt.rngtype;
  wid2.station="RNG";
  wid2.dt=opt.dt;

  aff::Series<double> series(opt.nsamples);
  for (int iseries=0; iseries<opt.nseries; iseries++)
  {
    if (opt.verbose)
    {
      cout << "produce series #" << iseries+1 << endl;
    }
    aff::Iterator<aff::Series<double> > CS(series);
    while (CS.valid())
    {
      *CS = rng.value();
      ++CS;
    }
    std::ostringstream oss;
    oss << iseries+1;
    wid2.channel=oss.str();
    os << wid2;
    os << series;
  }
}

/* ----- END OF randomseries.cc ----- */
