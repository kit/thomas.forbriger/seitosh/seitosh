Seitosh
=======
A Seismologist's Tool Shed

Seitosh is a collection of source code for program libraries 
and programs in seismic data processing, seismic data simulation 
and inversion. They are kept bundled in one repository due to 
their strong interrelation. For further information see the
accompanying [wiki](https://gitlab.kit.edu/kit/thomas.forbriger/seitosh/seitosh/-/wikis/home).

Copyright and License
---------------------
See [COPYING](COPYING) for copyright and license and disclaimer
of warranty. You are invited to fork this project and to provide
your own additions by sending patches or pull requests.
The home of the original Seitosh repository is 
https://gitlab.kit.edu/kit/thomas.forbriger/seitosh/seitosh

Installation
------------
See [INSTALL.md](INSTALL.md) for installation instructions.

Contents
--------
Contents of directories:
 - [src](src): All source code, README files describing the installation
   process and requiredments, and a shell script supporting the installation
   process can be found here.
 - [contrib](contrib): Additional files supporting the developers and the
   users of the code are provided in this subdirectory. 
   The files herein are no integral part of the Seitosh source code.

The contents of this repository previously were hosted 
in the TFSoftware repository. 
TFSoftware no longer is accessible from outside KIT.
See [README.history](README.history) for the history of 
this project.

News
----
Recent development (bug fixes, new features, etc) is reported in
[CHANGELOG](CHANGELOG).
